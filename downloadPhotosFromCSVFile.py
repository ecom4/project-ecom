import sys
import unicodecsv as csv
import numpy as np
import wget
import os

if (len(sys.argv) != 2):
    print("1 arg required")
    sys.exit()

imageDestination = "src/main/webapp/content/images/"
csvDestination = "src/main/resources/config/liquibase/data/product.csv"

fileExists = os.path.exists(csvDestination)

with open(csvDestination, 'a') as writeFile:
    reader = np.genfromtxt(sys.argv[1], dtype=None, encoding="UTF8", delimiter='\t', names=True)
    fieldnames = ["id", "name", "stock", "price", "discount", "image_url","description", "kilo_weight", "category_id"]
    writer = csv.DictWriter(writeFile, delimiter=';', quotechar='\"', fieldnames = fieldnames)
    if(not fileExists):
        writer.writeheader()
    for row in reader:
        wget.download(row["imageUrl"], out=imageDestination + str(row["id"]) + os.path.splitext(row["imageUrl"])[1])
        fileName = "content/images/" + str(row["id"]) + os.path.splitext(row["imageUrl"])[1]
        print(" -> " + row["name"])
        writer.writerow({"id": row["id"], "name": row["name"], "stock": row["stock"], "price": row["price"], "discount": row["discount"], "image_url": fileName,"description": row["description"] ,"kilo_weight": row["kiloWeight"], "category_id": row["category"]})





