package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ProductList;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.ProductListRepository;
import com.mycompany.myapp.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class ProductListService {

    private final ProductListRepository productListRepository;
    private final UserRepository userRepository;

    public ProductListService(ProductListRepository productListRepository, UserRepository userRepository){
        this.productListRepository = productListRepository;
        this.userRepository = userRepository;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void createNewProductListForUser(User user){
        ProductList newProductList = new ProductList();
        productListRepository.save(newProductList);

        /*
         * Need to refresh user here as the user will not be persisted without the password being set on the object
         * This is null on the User in relation to the POrder sent in an API request as it is marked with the @jsonignore tag for security
         */
        User refreshedUser = userRepository.findById(user.getId()).orElse(null);
        if(refreshedUser != null){
            refreshedUser.setCart(newProductList);
            userRepository.save(refreshedUser);

        }

    }
}
