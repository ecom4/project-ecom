package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.repository.PorderRepository;
import com.mycompany.myapp.repository.ProductLineRepository;
import com.mycompany.myapp.repository.ProductRepository;
import com.mycompany.myapp.repository.search.PorderSearchRepository;
import com.mycompany.myapp.repository.search.ProductSearchRepository;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class OrderProcessService {

    private final ProductLineRepository productLineRepository;
    private final ProductRepository productRepository;
    private final ProductSearchRepository productSearchRepository;
    private final PorderRepository porderRepository;
    private final PorderSearchRepository porderSearchRepository;
    private final ProductListService productListService;

    public OrderProcessService(ProductLineRepository productLineRepository, ProductRepository productRepository,
                               ProductSearchRepository productSearchRepository, PorderRepository porderRepository, PorderSearchRepository porderSearchRepository
                                , ProductListService productListService) {
        this.productLineRepository = productLineRepository;
        this.productRepository = productRepository;
        this.productSearchRepository = productSearchRepository;
        this.porderRepository = porderRepository;
        this.porderSearchRepository = porderSearchRepository;
        this.productListService = productListService;
    }

    /*
     * This method takes an unpersisted order and processes it, the steps for  processing are:
     * 1) Read each product in the order with a lock on the read function until the transaction is finished
     * 2) Check stock left for the product - if less than the order, add this information to the exception that will be thrown. If not reduce stock (will be rolled back if another product has this issue)
     *      - this exception does a rollback on any db changes in this method -> @Transactional(rollbackFor = {NotEnoughStockException.class})
     * 3) Persist purchase order with product list id
     * 4) Create new product list and associate this product list as the cart for the user (this is done in the method productListService.createNewProductListForUser
     */
    @Transactional(rollbackFor = {NotEnoughStockException.class})
    public Porder processOrder(Porder porder, Long cartId) throws NotEnoughStockException{
        List<ProductLine> upToDateProductLines = productLineRepository.findAllByProductListId(cartId);
        List<ProductQuantityExcess> productQuantityExcesses = new ArrayList<>();

        List<Long> productIdsToCheckForStock = new ArrayList<>();
        for (ProductLine productLine : upToDateProductLines) {
            if(productLine.getProduct() != null){
                productIdsToCheckForStock.add(productLine.getProduct().getId());
            }
        }
        List<Product> productsForStockUpdate = productRepository.findAllForStockChange(productIdsToCheckForStock);
        HashMap<Long, Product> productsForStockUpdateMap = convertListOfProductsToMap(productsForStockUpdate);

        for (ProductLine productLine : upToDateProductLines) {
//                Product productForStockUpdate = productRepository.findForStockChange(productLine.getProduct().getId());
            /*
             * Check if there is enough stock for product line, if not add product quantity excess object
             */
            if (productsForStockUpdateMap.containsKey(productLine.getProduct().getId())) {
                Product productForStockUpdate = productsForStockUpdateMap.get(productLine.getProduct().getId());
                int stockQuantity = productForStockUpdate.getStock();
                int productQuantityForOrder = productLine.getQuantity();
                if (stockQuantity >= productQuantityForOrder) {
                    productForStockUpdate.setStock(stockQuantity - productQuantityForOrder);
                    productRepository.save(productForStockUpdate);
                    productSearchRepository.save(productForStockUpdate);
                } else{
                    /*
                     * If quantity is too high for a product in an order create a ProductQuantityExcess exception
                     * This will be passed into an exception which allows us to see what products caused this problem
                     */
                    ProductQuantityExcess productQuantityExcess = new ProductQuantityExcess();
                    productQuantityExcess.setStockLeft(stockQuantity);
                    productQuantityExcess.setProduct(productForStockUpdate);
                    productQuantityExcess.setOldQuantity(productLine.getQuantity());
                    productQuantityExcess.setOriginalCost(productLine.getQuantity() * productLine.getUnitPrice());
                    productQuantityExcess.setRevisedCost(productForStockUpdate.getStock() * productLine.getUnitPrice());
                    productQuantityExcesses.add(productQuantityExcess);
                }
            }
        }
        if(productQuantityExcesses.size() > 0){
            throw new NotEnoughStockException(productQuantityExcesses);
        }

        // Save Porder
        Porder result = porderRepository.save(porder);
        porderSearchRepository.save(result);

        // Create new product list for cart and attach to user
        productListService.createNewProductListForUser(porder.getUser());


        return result;


    }

    private HashMap<Long, Product> convertListOfProductsToMap(List<Product> productList){
        HashMap<Long, Product> productMap = new HashMap<>();
        for(Product product: productList){
            productMap.put(product.getId(), product);
        }
        return productMap;
    }

}
