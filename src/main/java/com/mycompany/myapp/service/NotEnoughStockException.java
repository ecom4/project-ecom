package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ProductQuantityExcess;
import com.mycompany.myapp.web.rest.errors.ErrorConstants;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.List;

public class NotEnoughStockException extends RuntimeException {

    private final List<ProductQuantityExcess> productQuantityExcesses;


    private static final long serialVersionUID = 1L;

    public NotEnoughStockException(List<ProductQuantityExcess> productQuantityExcesses) {
        super("Not enough stock");
        this.productQuantityExcesses = productQuantityExcesses;
    }
//    public NotEnoughStockWebException(String defaultMessage, List<ProductQuantityExcess> productQuantityExcesses) {
//        super(defaultMessage);
//        this.productQuantityExcesses = productQuantityExcesses;
//    }

    public List<ProductQuantityExcess> getProductQuantityExcesses() {
        return productQuantityExcesses;
    }
}
