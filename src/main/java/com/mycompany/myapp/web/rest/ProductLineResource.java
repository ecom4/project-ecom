package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.ProductLine;
import com.mycompany.myapp.repository.ProductLineRepository;
import com.mycompany.myapp.repository.search.ProductLineSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ProductLine}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductLineResource {

    private final Logger log = LoggerFactory.getLogger(ProductLineResource.class);

    private static final String ENTITY_NAME = "productLine";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductLineRepository productLineRepository;

    private final ProductLineSearchRepository productLineSearchRepository;

    public ProductLineResource(ProductLineRepository productLineRepository, ProductLineSearchRepository productLineSearchRepository) {
        this.productLineRepository = productLineRepository;
        this.productLineSearchRepository = productLineSearchRepository;
    }

    /**
     * {@code POST  /product-lines} : Create a new productLine.
     *
     * @param productLine the productLine to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productLine, or with status {@code 400 (Bad Request)} if the productLine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-lines")
    public ResponseEntity<ProductLine> createProductLine(@Valid @RequestBody ProductLine productLine) throws URISyntaxException {
        log.debug("REST request to save ProductLine : {} in productList : {}", productLine, productLine.getProductList());
        if (productLine.getId() != null) {
            throw new BadRequestAlertException("A new productLine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductLine result = productLineRepository.save(productLine);
        productLineSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/product-lines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-lines} : Updates an existing productLine.
     *
     * @param productLine the productLine to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productLine,
     * or with status {@code 400 (Bad Request)} if the productLine is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productLine couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-lines")
    public ResponseEntity<ProductLine> updateProductLine(@Valid @RequestBody ProductLine productLine) throws URISyntaxException {
        log.debug("REST request to update ProductLine : {}", productLine);
        if (productLine.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductLine result = productLineRepository.save(productLine);
        productLineSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productLine.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-lines} : get all the productLines.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productLines in body.
     */
    @GetMapping("/product-lines")
    public List<ProductLine> getAllProductLines() {
        log.debug("REST request to get all ProductLines");
        return productLineRepository.findAll();
    }

    /**
     * {@code GET  /product-lines/:id} : get the "id" productLine.
     *
     * @param id the id of the productLine to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productLine, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-lines/{id}")
    public ResponseEntity<ProductLine> getProductLine(@PathVariable Long id) {
        log.debug("REST request to get ProductLine : {}", id);
        Optional<ProductLine> productLine = productLineRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productLine);
    }

    /**
     * {@code DELETE  /product-lines/:id} : delete the "id" productLine.
     *
     * @param id the id of the productLine to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-lines/{id}")
    public ResponseEntity<Void> deleteProductLine(@PathVariable Long id) {
        log.debug("REST request to delete ProductLine : {}", id);
        productLineRepository.deleteById(id);
        productLineSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/product-lines?query=:query} : search for the productLine corresponding
     * to the query.
     *
     * @param query the query of the productLine search.
     * @return the result of the search.
     */
    @GetMapping("/_search/product-lines")
    public List<ProductLine> searchProductLines(@RequestParam String query) {
        log.debug("REST request to search ProductLines for query {}", query);
        return StreamSupport
            .stream(productLineSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
