package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Paramaters;
import com.mycompany.myapp.repository.ParamatersRepository;
import com.mycompany.myapp.repository.search.ParamatersSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Paramaters}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ParamatersResource {

    private final Logger log = LoggerFactory.getLogger(ParamatersResource.class);

    private static final String ENTITY_NAME = "paramaters";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParamatersRepository paramatersRepository;

    private final ParamatersSearchRepository paramatersSearchRepository;

    public ParamatersResource(ParamatersRepository paramatersRepository, ParamatersSearchRepository paramatersSearchRepository) {
        this.paramatersRepository = paramatersRepository;
        this.paramatersSearchRepository = paramatersSearchRepository;
    }

    /**
     * {@code POST  /paramaters} : Create a new paramaters.
     *
     * @param paramaters the paramaters to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paramaters, or with status {@code 400 (Bad Request)} if the paramaters has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/paramaters")
    public ResponseEntity<Paramaters> createParamaters(@Valid @RequestBody Paramaters paramaters) throws URISyntaxException {
        log.debug("REST request to save Paramaters : {}", paramaters);
        if (paramaters.getId() != null) {
            throw new BadRequestAlertException("A new paramaters cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Paramaters result = paramatersRepository.save(paramaters);
        paramatersSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/paramaters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /paramaters} : Updates an existing paramaters.
     *
     * @param paramaters the paramaters to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paramaters,
     * or with status {@code 400 (Bad Request)} if the paramaters is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paramaters couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/paramaters")
    public ResponseEntity<Paramaters> updateParamaters(@Valid @RequestBody Paramaters paramaters) throws URISyntaxException {
        log.debug("REST request to update Paramaters : {}", paramaters);
        if (paramaters.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Paramaters result = paramatersRepository.save(paramaters);
        paramatersSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paramaters.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /paramaters} : get all the paramaters.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paramaters in body.
     */
    @GetMapping("/paramaters")
    public List<Paramaters> getAllParamaters() {
        log.debug("REST request to get all Paramaters");
        return paramatersRepository.findAll();
    }

    /**
     * {@code GET  /paramaters/:id} : get the "id" paramaters.
     *
     * @param id the id of the paramaters to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paramaters, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/paramaters/{id}")
    public ResponseEntity<Paramaters> getParamaters(@PathVariable Long id) {
        log.debug("REST request to get Paramaters : {}", id);
        Optional<Paramaters> paramaters = paramatersRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(paramaters);
    }

    /**
     * {@code DELETE  /paramaters/:id} : delete the "id" paramaters.
     *
     * @param id the id of the paramaters to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/paramaters/{id}")
    public ResponseEntity<Void> deleteParamaters(@PathVariable Long id) {
        log.debug("REST request to delete Paramaters : {}", id);
        paramatersRepository.deleteById(id);
        paramatersSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/paramaters?query=:query} : search for the paramaters corresponding
     * to the query.
     *
     * @param query the query of the paramaters search.
     * @return the result of the search.
     */
    @GetMapping("/_search/paramaters")
    public List<Paramaters> searchParamaters(@RequestParam String query) {
        log.debug("REST request to search Paramaters for query {}", query);
        return StreamSupport
            .stream(paramatersSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
