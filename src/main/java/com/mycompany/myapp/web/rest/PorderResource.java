package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Porder;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.PorderRepository;
import com.mycompany.myapp.repository.UserRepository;

import com.mycompany.myapp.domain.ProductQuantityExcess;

import com.mycompany.myapp.repository.search.PorderSearchRepository;
import com.mycompany.myapp.service.NotEnoughStockException;
import com.mycompany.myapp.service.OrderProcessService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import com.mycompany.myapp.web.rest.errors.NotEnoughStockWebException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Porder}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PorderResource {

    private final Logger log = LoggerFactory.getLogger(PorderResource.class);

    private static final String ENTITY_NAME = "porder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PorderRepository porderRepository;
    
    private final UserRepository userRepository;

    private final PorderSearchRepository porderSearchRepository;

    private final OrderProcessService orderProcessService;


    public PorderResource(PorderRepository porderRepository, UserRepository userRepository, PorderSearchRepository porderSearchRepository, OrderProcessService orderProcessService) {
        this.porderRepository = porderRepository;
        this.userRepository = userRepository;
        this.porderSearchRepository = porderSearchRepository;
        this.orderProcessService = orderProcessService;
    }


    // Default used by jhipster - doesn't check stock
//    /**
//     * {@code POST  /porders} : Create a new porder.
//     *
//     * @param porder the porder to create.
//     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new porder, or with status {@code 400 (Bad Request)} if the porder has already an ID.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PostMapping("/porders")
//    public ResponseEntity<Porder> createPorder(@Valid @RequestBody Porder porder) throws URISyntaxException {
//        log.debug("REST request to save Porder : {}", porder);
//        if (porder.getId() != null) {
//            throw new BadRequestAlertException("A new porder cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        Porder result = porderRepository.save(porder);
//        porderSearchRepository.save(result);
//        return ResponseEntity.created(new URI("/api/porders/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }
//
    /**
     * {@code POST  /porders} : Create a new porder.
     *
     * @param porder the porder to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new porder, or with status {@code 400 (Bad Request)} if the porder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/porders")
    public ResponseEntity<Porder> createPorder(@Valid @RequestBody Porder porder) throws URISyntaxException {
        log.debug("REST request to save Porder : {}", porder);
        if (porder.getId() != null) {
            throw new BadRequestAlertException( "Une nouvelle commande ne peut pas déjà avoir un identifiant", ENTITY_NAME, "idexists");
        }
        // if porder has cart with products process order
        if(null != porder.getCart() && null != porder.getCart().getId() && !porder.getCart().getProductLines().isEmpty()){
            try{
                Porder result = orderProcessService.processOrder(porder, porder.getCart().getId());
                return ResponseEntity.created(new URI("/api/porders/" + result.getId()))
//                    .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                    .headers(HeaderUtil.createAlert(applicationName, "ecomApp.porder.orderCreated", ""))
                    .body(result);
            } catch (NotEnoughStockException notEnoughStockException){
                for(ProductQuantityExcess productQuantityExcess: notEnoughStockException.getProductQuantityExcesses()){
                    System.out.println("stock issue - excess quantity = " + productQuantityExcess.getStockLeft());
                }
                throw new NotEnoughStockWebException(notEnoughStockException.getProductQuantityExcesses()); // Cannot process order without products
            }
        } else{
            throw new BadRequestAlertException("La commande doit contenir des produits", ENTITY_NAME, "ecomApp.porder.error.noProducts"); // Cannot process order without products
        }


    }

    /**
     * {@code PUT  /porders} : Updates an existing porder.
     *
     * @param porder the porder to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated porder,
     * or with status {@code 400 (Bad Request)} if the porder is not valid,
     * or with status {@code 500 (Internal Server Error)} if the porder couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/porders")
    public ResponseEntity<Porder> updatePorder(@Valid @RequestBody Porder porder) throws URISyntaxException {
        log.debug("REST request to update Porder : {}", porder);
        if (porder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Porder result = porderRepository.save(porder);
        porderSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, porder.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /porders} : get all the porders.
     *
     * @param user : [userid] if set, returns the orders for this user
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of porders in body.
     */
    @GetMapping("/porders")
    public List<Porder> getAllPorders(@RequestParam(required = false, name="user") Long userId) {
        log.debug("REST request to get all Porders");
        if(userId == null){
            return porderRepository.findAll();
        } else {
            Optional<User> u = userRepository.findById(userId);
            if(u.isPresent()){
                return porderRepository.findAllByUser(u);
            } else {
                return porderRepository.findAll();
            }
        }
    }

    /**
     * {@code GET  /porders/:id} : get the "id" porder.
     *
     * @param id the id of the porder to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the porder, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/porders/{id}")
    public ResponseEntity<Porder> getPorder(@PathVariable Long id) {
        log.debug("REST request to get Porder : {}", id);
        Optional<Porder> porder = porderRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(porder);
    }

    /**
     * {@code DELETE  /porders/:id} : delete the "id" porder.
     *
     * @param id the id of the porder to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/porders/{id}")
    public ResponseEntity<Void> deletePorder(@PathVariable Long id) {
        log.debug("REST request to delete Porder : {}", id);
        porderRepository.deleteById(id);
        porderSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/porders?query=:query} : search for the porder corresponding
     * to the query.
     *
     * @param query the query of the porder search.
     * @return the result of the search.
     */
    @GetMapping("/_search/porders")
    public List<Porder> searchPorders(@RequestParam String query) {
        log.debug("REST request to search Porders for query {}", query);
        return StreamSupport
            .stream(porderSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
