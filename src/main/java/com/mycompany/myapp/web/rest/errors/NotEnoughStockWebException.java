package com.mycompany.myapp.web.rest.errors;

import com.mycompany.myapp.domain.ProductQuantityExcess;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.List;

public class NotEnoughStockWebException extends AbstractThrowableProblem {

    private final List<ProductQuantityExcess> productQuantityExcesses;


    private static final long serialVersionUID = 1L;

    public NotEnoughStockWebException(List<ProductQuantityExcess> productQuantityExcesses) {
        super(ErrorConstants.DEFAULT_TYPE, "Not enough stock", Status.BAD_REQUEST);
        this.productQuantityExcesses = productQuantityExcesses;
    }

    public List<ProductQuantityExcess> getProductQuantityExcesses() {
        return productQuantityExcesses;
    }
}
