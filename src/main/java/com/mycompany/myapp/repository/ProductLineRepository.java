package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ProductLine;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data  repository for the ProductLine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {
    // Find total cost by summing the quantity by the unit price for a product line. These two fields are not null (see entity)
    @Query("select sum(pl.quantity*pl.unitPrice) from ProductLine pl left join pl.productList prodList " +
        "where prodList.id =:productListId")
    Float findTotalCostForProductList(@Param("productListId") Long productListId);


    @Query("select pl from ProductLine pl left join pl.productList prodList where prodList.id =:productListId")
    List<ProductLine> findAllByProductListId(@Param("productListId") Long productListId);

}
