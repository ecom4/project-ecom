package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Category;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    
    @Query("select distinct category from Category category left join fetch category.childCategories where category.parentCategory is null")
    List<Category> findAllRoot();
}
