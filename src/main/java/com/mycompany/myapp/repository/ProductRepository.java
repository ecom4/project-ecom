package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Product entity.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


    @Query(value = "select distinct product from Product product left join fetch product.tags",
        countQuery = "select count(distinct product) from Product product")
    Page<Product> findAllWithEagerRelationships(Pageable pageable);

    Page<Product> findByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Product> findByCategoryId(long id, Pageable pageable);

    @Query("select distinct product from Product product left join fetch product.tags")
    List<Product> findAllWithEagerRelationships();

    @Query("select product from Product product left join fetch product.tags where product.id =:id")
    Optional<Product> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select product from Product product where product.stock < 10 order by product.stock asc ")
    List<Product> findAllWithLowStock();

//    @Lock(LockModeType.PESSIMISTIC_READ)
//    @Query("select p from Product p where p.id = :id")
//    Product findForStockChange(@Param("id") Long id);

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("select p from Product p where p.id in (:productIds)")
    List<Product> findAllForStockChange(@Param("productIds") List<Long> productIds);

}
