package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Paramaters;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Paramaters entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParamatersRepository extends JpaRepository<Paramaters, Long> {
}
