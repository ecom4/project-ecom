package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Porder;
import com.mycompany.myapp.domain.User;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Porder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PorderRepository extends JpaRepository<Porder, Long> {

    @Query("select p from Porder p where p.user = :user order by p.date desc ")
    List<Porder> findAllByUser(@Param("user") Optional<User> user);
}
