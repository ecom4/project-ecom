package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.ProductList;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ProductList} entity.
 */
public interface ProductListSearchRepository extends ElasticsearchRepository<ProductList, Long> {
}
