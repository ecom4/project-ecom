package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.ProductLine;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ProductLine} entity.
 */
public interface ProductLineSearchRepository extends ElasticsearchRepository<ProductLine, Long> {
}
