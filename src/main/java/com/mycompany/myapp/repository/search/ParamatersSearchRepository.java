package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Paramaters;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Paramaters} entity.
 */
public interface ParamatersSearchRepository extends ElasticsearchRepository<Paramaters, Long> {
}
