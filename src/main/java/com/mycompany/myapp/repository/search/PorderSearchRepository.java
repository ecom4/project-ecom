package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Porder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Porder} entity.
 */
public interface PorderSearchRepository extends ElasticsearchRepository<Porder, Long> {
}
