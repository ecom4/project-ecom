package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A Paramaters.
 */
@Entity
@Table(name = "paramaters")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "paramaters")
public class Paramaters implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name_description", nullable = false , columnDefinition="TEXT")
    private String nameDescription;

    @NotNull
    @Column(name = "delivery_fees", nullable = false)
    private Double deliveryFees;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameDescription() {
        return nameDescription;
    }

    public Paramaters nameDescription(String nameDescription) {
        this.nameDescription = nameDescription;
        return this;
    }

    public void setNameDescription(String nameDescription) {
        this.nameDescription = nameDescription;
    }

    public Double getDeliveryFees() {
        return deliveryFees;
    }

    public Paramaters deliveryFees(Double deliveryFees) {
        this.deliveryFees = deliveryFees;
        return this;
    }

    public void setDeliveryFees(Double deliveryFees) {
        this.deliveryFees = deliveryFees;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Paramaters)) {
            return false;
        }
        return id != null && id.equals(((Paramaters) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Paramaters{" +
            "id=" + getId() +
            ", nameDescription='" + getNameDescription() + "'" +
            ", deliveryFees=" + getDeliveryFees() +
            "}";
    }
}
