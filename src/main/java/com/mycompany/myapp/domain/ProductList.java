package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductList.
 */
@Entity
@Table(name = "product_list")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "productlist")
public class ProductList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @OneToMany(mappedBy = "productList", fetch=FetchType.EAGER, cascade={CascadeType.ALL}, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ProductLine> productLines = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = {"customLists", "cart"}, allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ProductLine> getProductLines() {
        return productLines;
    }

    public ProductList productLines(Set<ProductLine> productLines) {
        this.productLines = productLines;
        return this;
    }

    public ProductList addProductLines(ProductLine productLine) {
        this.productLines.add(productLine);
        productLine.setProductList(this);
        return this;
    }

    public ProductList removeProductLines(ProductLine productLine) {
        this.productLines.remove(productLine);
        productLine.setProductList(null);
        return this;
    }

    public void setProductLines(Set<ProductLine> productLines) {
        this.productLines = productLines;
    }

    public User getUser() {
        return user;
    }

    public ProductList user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductList)) {
            return false;
        }
        return id != null && id.equals(((ProductList) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductList{" +
            "id=" + getId() +
            "}";
    }

    public void dismissProductLine(ProductLine productLine) {
        this.productLines.remove(productLine);
    }

    public void dismissProductLines() {
        this.productLines.forEach(productLine -> productLine.dismissProductList()); // SYNCHRONIZING THE OTHER SIDE OF RELATIONSHIP
        this.productLines.clear();
    }

}
