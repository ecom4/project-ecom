package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Porder.
 */
@Entity
@Table(name = "porder")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "porder")
public class Porder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "date")
    private Instant date;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "delivery_type")
    private String deliveryType;

    @Column(name = "status")
    private String status;

    @DecimalMin(value = "0.0")
    @Column(name = "total_price")
    private Float totalPrice;

    @DecimalMin(value = "0.0")
    @Column(name = "delivery_fees")
    private Float deliveryFees;

    @OneToOne
    @JoinColumn(unique = true)
    private ProductList cart;

    @ManyToOne
    @JsonIgnoreProperties(value = {"orderHistories"}, allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public Porder date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public Porder paymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public Porder deliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
        return this;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getStatus() {
        return status;
    }

    public Porder status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getTotalPrice() {
        return totalPrice;
    }

    public Porder totalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Float getDeliveryFees() {
        return deliveryFees;
    }

    public Porder deliveryFees(Float deliveryFees) {
        this.deliveryFees = deliveryFees;
        return this;
    }

    public void setDeliveryFees(Float deliveryFees) {
        this.deliveryFees = deliveryFees;
    }

    public ProductList getCart() {
        return cart;
    }

    public Porder cart(ProductList productList) {
        this.cart = productList;
        return this;
    }

    public void setCart(ProductList productList) {
        this.cart = productList;
    }

    public User getUser() {
        return user;
    }

    public Porder user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @PrePersist
    public void setDateOnCreation(){
        this.setDate(Instant.now());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Porder)) {
            return false;
        }
        return id != null && id.equals(((Porder) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Porder{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", paymentType='" + getPaymentType() + "'" +
            ", deliveryType='" + getDeliveryType() + "'" +
            ", status='" + getStatus() + "'" +
            ", totalPrice=" + getTotalPrice() +
            ", deliveryFees=" + getDeliveryFees() +
            "}";
    }
}
