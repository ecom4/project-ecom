package com.mycompany.myapp.domain;

public class ProductQuantityExcess {
    private Product product;
    private int stockLeft;
    private int oldQuantity;

    private float originalCost;
    private float revisedCost;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getStockLeft() {
        return stockLeft;
    }

    public void setStockLeft(int stockLeft) {
        this.stockLeft = stockLeft;
    }

    public float getOriginalCost() {
        return originalCost;
    }

    public void setOriginalCost(float originalCost) {
        this.originalCost = originalCost;
    }

    public float getRevisedCost() {
        return revisedCost;
    }

    public void setRevisedCost(float revisedCost) {
        this.revisedCost = revisedCost;
    }

    public int getOldQuantity() {
        return this.oldQuantity;
    }

    public void setOldQuantity(int oldQuantity) {
        this.oldQuantity = oldQuantity;
    }
}
