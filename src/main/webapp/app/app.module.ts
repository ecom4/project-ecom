/* eslint-disable */
import { CommonModule } from '@angular/common';
import { EcomOrderModule } from './order/order.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { NumberPickerModule } from 'ng-number-picker';
import './vendor';
import { EcomSharedModule } from 'app/shared/shared.module';
import { EcomCoreModule } from 'app/core/core.module';
import { EcomAppRoutingModule } from './app-routing.module';
import { EcomHomeModule } from './home/home.module';
import { EcomProductModule } from './product/product.module';
import { EcomEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { CartComponent } from './cart/cart.component';
import { CartConfirmDeleteDialogComponent } from 'app/cart/cart-confirm-delete-dialog.component';
import { NavbarUserComponent } from './layouts/navbar-user/navbar-user.component';
import { DescriptionComponent } from './description/description.component';
import { PaymentComponent } from './payment/payment.component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MenuComponent } from './layouts/menu/menu.component';
import { ShoppingCartSummaryComponent } from './shopping-cart-summary/shopping-cart-summary.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as Cloudinary from 'cloudinary-core';
import { PaymentStockIssueDialogComponent } from 'app/payment/payment-stock-issue-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    CloudinaryModule.forRoot(Cloudinary, {
      cloud_name: 'ecomdrive',
      upload_preset: 'ml_default',
      secure: true,
      api_key: '696433798311419',
      api_secret: 'Pfa2ie-S9Bon-Owyz1bWk3GJtxw',
    }),
    EcomSharedModule,
    EcomCoreModule,
    EcomHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    EcomEntityModule,
    EcomAppRoutingModule,
    EcomOrderModule,
    FontAwesomeModule,
    MatSidenavModule,
    NumberPickerModule,
    EcomProductModule,
    FormsModule,
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    ProductDetailsComponent,
    NavbarUserComponent,
    DescriptionComponent,
    CartComponent,
    CartConfirmDeleteDialogComponent,
    MenuComponent,
    PaymentComponent,
    ShoppingCartSummaryComponent,
    PaymentStockIssueDialogComponent,
  ],
  bootstrap: [MainComponent],
  exports: [ProductDetailsComponent],
})
export class EcomAppModule {}
