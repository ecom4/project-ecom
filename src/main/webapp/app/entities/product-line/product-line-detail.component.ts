import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductLine } from 'app/shared/model/product-line.model';

@Component({
  selector: 'jhi-product-line-detail',
  templateUrl: './product-line-detail.component.html',
})
export class ProductLineDetailComponent implements OnInit {
  productLine: IProductLine | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productLine }) => (this.productLine = productLine));
  }

  previousState(): void {
    window.history.back();
  }
}
