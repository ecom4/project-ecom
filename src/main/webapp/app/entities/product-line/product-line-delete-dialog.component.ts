import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductLine } from 'app/shared/model/product-line.model';
import { ProductLineService } from './product-line.service';

@Component({
  templateUrl: './product-line-delete-dialog.component.html',
})
export class ProductLineDeleteDialogComponent {
  productLine?: IProductLine;

  constructor(
    protected productLineService: ProductLineService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productLineService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productLineListModification');
      this.activeModal.close();
    });
  }
}
