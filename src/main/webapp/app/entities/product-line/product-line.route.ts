import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductLine, ProductLine } from 'app/shared/model/product-line.model';
import { ProductLineService } from './product-line.service';
import { ProductLineComponent } from './product-line.component';
import { ProductLineDetailComponent } from './product-line-detail.component';
import { ProductLineUpdateComponent } from './product-line-update.component';

@Injectable({ providedIn: 'root' })
export class ProductLineResolve implements Resolve<IProductLine> {
  constructor(private service: ProductLineService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductLine> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productLine: HttpResponse<ProductLine>) => {
          if (productLine.body) {
            return of(productLine.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductLine());
  }
}

export const productLineRoute: Routes = [
  {
    path: '',
    component: ProductLineComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.productLine.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductLineDetailComponent,
    resolve: {
      productLine: ProductLineResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.productLine.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductLineUpdateComponent,
    resolve: {
      productLine: ProductLineResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.productLine.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductLineUpdateComponent,
    resolve: {
      productLine: ProductLineResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.productLine.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
