import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductLine, ProductLine } from 'app/shared/model/product-line.model';
import { ProductLineService } from './product-line.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { IProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from 'app/entities/product-list/product-list.service';

type SelectableEntity = IProduct | IProductList;

@Component({
  selector: 'jhi-product-line-update',
  templateUrl: './product-line-update.component.html',
})
export class ProductLineUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];
  productlists: IProductList[] = [];

  editForm = this.fb.group({
    id: [],
    quantity: [null, [Validators.required, Validators.min(0)]],
    unitPrice: [null, [Validators.required, Validators.min(0.0)]],
    product: [null, Validators.required],
    productList: [],
  });

  constructor(
    protected productLineService: ProductLineService,
    protected productService: ProductService,
    protected productListService: ProductListService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productLine }) => {
      this.updateForm(productLine);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));

      this.productListService.query().subscribe((res: HttpResponse<IProductList[]>) => (this.productlists = res.body || []));
    });
  }

  updateForm(productLine: IProductLine): void {
    this.editForm.patchValue({
      id: productLine.id,
      quantity: productLine.quantity,
      unitPrice: productLine.unitPrice,
      product: productLine.product,
      productList: productLine.productList,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productLine = this.createFromForm();
    if (productLine.id !== undefined) {
      this.subscribeToSaveResponse(this.productLineService.update(productLine));
    } else {
      this.subscribeToSaveResponse(this.productLineService.create(productLine));
    }
  }

  private createFromForm(): IProductLine {
    return {
      ...new ProductLine(),
      id: this.editForm.get(['id'])!.value,
      quantity: this.editForm.get(['quantity'])!.value,
      unitPrice: this.editForm.get(['unitPrice'])!.value,
      product: this.editForm.get(['product'])!.value,
      productList: this.editForm.get(['productList'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductLine>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
