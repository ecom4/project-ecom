import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductLine } from 'app/shared/model/product-line.model';
import { ProductLineService } from './product-line.service';
import { ProductLineDeleteDialogComponent } from './product-line-delete-dialog.component';

@Component({
  selector: 'jhi-product-line',
  templateUrl: './product-line.component.html',
})
export class ProductLineComponent implements OnInit, OnDestroy {
  productLines?: IProductLine[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected productLineService: ProductLineService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.productLineService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IProductLine[]>) => (this.productLines = res.body || []));
      return;
    }

    this.productLineService.query().subscribe((res: HttpResponse<IProductLine[]>) => (this.productLines = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductLines();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductLine): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductLines(): void {
    this.eventSubscriber = this.eventManager.subscribe('productLineListModification', () => this.loadAll());
  }

  delete(productLine: IProductLine): void {
    const modalRef = this.modalService.open(ProductLineDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productLine = productLine;
  }
}
