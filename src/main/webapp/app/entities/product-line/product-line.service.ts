import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IProductLine } from 'app/shared/model/product-line.model';

type EntityResponseType = HttpResponse<IProductLine>;
type EntityArrayResponseType = HttpResponse<IProductLine[]>;

@Injectable({ providedIn: 'root' })
export class ProductLineService {
  public resourceUrl = SERVER_API_URL + 'api/product-lines';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/product-lines';

  constructor(protected http: HttpClient) {}

  create(productLine: IProductLine): Observable<EntityResponseType> {
    return this.http.post<IProductLine>(this.resourceUrl, productLine, { observe: 'response' });
  }

  update(productLine: IProductLine): Observable<EntityResponseType> {
    return this.http.put<IProductLine>(this.resourceUrl, productLine, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductLine>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductLine[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductLine[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
