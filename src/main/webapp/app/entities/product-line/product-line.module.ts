import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomSharedModule } from 'app/shared/shared.module';
import { ProductLineComponent } from './product-line.component';
import { ProductLineDetailComponent } from './product-line-detail.component';
import { ProductLineUpdateComponent } from './product-line-update.component';
import { ProductLineDeleteDialogComponent } from './product-line-delete-dialog.component';
import { productLineRoute } from './product-line.route';

@NgModule({
  imports: [EcomSharedModule, RouterModule.forChild(productLineRoute)],
  declarations: [ProductLineComponent, ProductLineDetailComponent, ProductLineUpdateComponent, ProductLineDeleteDialogComponent],
  entryComponents: [ProductLineDeleteDialogComponent],
})
export class EcomProductLineModule {}
