import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFeedBack, FeedBack } from 'app/shared/model/feed-back.model';
import { FeedBackService } from './feed-back.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service.ts';

@Component({
  selector: 'jhi-feed-back-update',
  templateUrl: './feed-back-update.component.html',
})
export class FeedBackUpdateComponent implements OnInit {
  isSaving = false;
  extendedusers: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    message: [null, [Validators.required]],
    read: [],
    user: [],
  });

  constructor(
    protected feedBackService: FeedBackService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ feedBack }) => {
      this.updateForm(feedBack);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.extendedusers = res.body || []));
    });
  }

  updateForm(feedBack: IFeedBack): void {
    this.editForm.patchValue({
      id: feedBack.id,
      message: feedBack.message,
      read: feedBack.read,
      user: feedBack.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const feedBack = this.createFromForm();
    if (feedBack.id !== undefined) {
      this.subscribeToSaveResponse(this.feedBackService.update(feedBack));
    } else {
      this.subscribeToSaveResponse(this.feedBackService.create(feedBack));
    }
  }

  private createFromForm(): IFeedBack {
    return {
      ...new FeedBack(),
      id: this.editForm.get(['id'])!.value,
      message: this.editForm.get(['message'])!.value,
      read: this.editForm.get(['read'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFeedBack>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
