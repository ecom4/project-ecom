import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFeedBack } from 'app/shared/model/feed-back.model';
import { FeedBackService } from './feed-back.service';
import { FeedBackDeleteDialogComponent } from './feed-back-delete-dialog.component';

@Component({
  selector: 'jhi-feed-back',
  templateUrl: './feed-back.component.html',
})
export class FeedBackComponent implements OnInit, OnDestroy {
  feedBacks?: IFeedBack[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected feedBackService: FeedBackService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.feedBackService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IFeedBack[]>) => (this.feedBacks = res.body || []));
      return;
    }

    this.feedBackService.query().subscribe((res: HttpResponse<IFeedBack[]>) => (this.feedBacks = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFeedBacks();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFeedBack): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFeedBacks(): void {
    this.eventSubscriber = this.eventManager.subscribe('feedBackListModification', () => this.loadAll());
  }

  delete(feedBack: IFeedBack): void {
    const modalRef = this.modalService.open(FeedBackDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.feedBack = feedBack;
  }
}
