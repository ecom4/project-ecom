import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomSharedModule } from 'app/shared/shared.module';
import { ProductListComponent } from './product-list.component';
import { ProductListDetailComponent } from './product-list-detail.component';
import { ProductListUpdateComponent } from './product-list-update.component';
import { ProductListDeleteDialogComponent } from './product-list-delete-dialog.component';
import { productListRoute } from './product-list.route';

@NgModule({
  imports: [EcomSharedModule, RouterModule.forChild(productListRoute)],
  declarations: [ProductListComponent, ProductListDetailComponent, ProductListUpdateComponent, ProductListDeleteDialogComponent],
  entryComponents: [ProductListDeleteDialogComponent],
})
export class EcomProductListModule {}
