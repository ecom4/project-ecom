import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from './product-list.service';
import { ProductListDeleteDialogComponent } from './product-list-delete-dialog.component';

@Component({
  selector: 'jhi-product-list',
  templateUrl: './product-list.component.html',
})
export class ProductListComponent implements OnInit, OnDestroy {
  productLists?: IProductList[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected productListService: ProductListService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.productListService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IProductList[]>) => (this.productLists = res.body || []));
      return;
    }

    this.productListService.query().subscribe((res: HttpResponse<IProductList[]>) => (this.productLists = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductLists();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductList): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductLists(): void {
    this.eventSubscriber = this.eventManager.subscribe('productListListModification', () => this.loadAll());
  }

  delete(productList: IProductList): void {
    const modalRef = this.modalService.open(ProductListDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productLines = productList;
  }
}
