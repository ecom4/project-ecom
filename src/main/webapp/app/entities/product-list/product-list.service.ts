import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IProductList } from 'app/shared/model/product-list.model';

type EntityResponseType = HttpResponse<IProductList>;
type EntityArrayResponseType = HttpResponse<IProductList[]>;

@Injectable({ providedIn: 'root' })
export class ProductListService {
  public resourceUrl = SERVER_API_URL + 'api/product-lists';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/product-lists';

  constructor(protected http: HttpClient) {}

  create(productList: IProductList): Observable<EntityResponseType> {
    return this.http.post<IProductList>(this.resourceUrl, productList, { observe: 'response' });
  }

  update(productList: IProductList): Observable<EntityResponseType> {
    return this.http.put<IProductList>(this.resourceUrl, productList, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductList>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductList[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  updateRemoveProductLines(productList: IProductList): Observable<HttpResponse<{}>> {
    return this.http.put<IProductList>(`${this.resourceUrl}/remove-product-lines`, productList, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductList[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  // getProductListTotalCost(productListId: number): Observable<number> {
  //   return this.http.get<number>(`${this.resourceUrl}/total-cost/${productListId}`, { observe: 'response' });
  // }

  getProductListTotalCost(productListId: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}/total-cost/${productListId}`, { observe: 'response' });
  }
}
