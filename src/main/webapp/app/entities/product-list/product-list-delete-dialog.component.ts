import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from './product-list.service';

@Component({
  templateUrl: './product-list-delete-dialog.component.html',
})
export class ProductListDeleteDialogComponent {
  productList?: IProductList;

  constructor(
    protected productListService: ProductListService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productListService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productListListModification');
      this.activeModal.close();
    });
  }
}
