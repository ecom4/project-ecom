import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductList, ProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from './product-list.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service.ts';

@Component({
  selector: 'jhi-product-list-update',
  templateUrl: './product-list-update.component.html',
})
export class ProductListUpdateComponent implements OnInit {
  isSaving = false;
  extendedusers: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    user: [],
  });

  constructor(
    protected productListService: ProductListService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productList }) => {
      this.updateForm(productList);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.extendedusers = res.body || []));
    });
  }

  updateForm(productList: IProductList): void {
    this.editForm.patchValue({
      id: productList.id,
      user: productList.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productList = this.createFromForm();
    if (productList.id !== undefined) {
      this.subscribeToSaveResponse(this.productListService.update(productList));
    } else {
      this.subscribeToSaveResponse(this.productListService.create(productList));
    }
  }

  private createFromForm(): IProductList {
    return {
      ...new ProductList(),
      id: this.editForm.get(['id'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductList>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
