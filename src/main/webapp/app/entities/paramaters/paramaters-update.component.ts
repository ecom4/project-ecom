import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IParamaters, Paramaters } from 'app/shared/model/paramaters.model';
import { ParamatersService } from './paramaters.service';

@Component({
  selector: 'jhi-paramaters-update',
  templateUrl: './paramaters-update.component.html',
})
export class ParamatersUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nameDescription: [null, [Validators.required]],
    deliveryFees: [null, [Validators.required]],
  });

  constructor(protected paramatersService: ParamatersService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paramaters }) => {
      this.updateForm(paramaters);
    });
  }

  updateForm(paramaters: IParamaters): void {
    this.editForm.patchValue({
      id: paramaters.id,
      nameDescription: paramaters.nameDescription,
      deliveryFees: paramaters.deliveryFees,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const paramaters = this.createFromForm();
    if (paramaters.id !== undefined) {
      this.subscribeToSaveResponse(this.paramatersService.update(paramaters));
    } else {
      this.subscribeToSaveResponse(this.paramatersService.create(paramaters));
    }
  }

  private createFromForm(): IParamaters {
    return {
      ...new Paramaters(),
      id: this.editForm.get(['id'])!.value,
      nameDescription: this.editForm.get(['nameDescription'])!.value,
      deliveryFees: this.editForm.get(['deliveryFees'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParamaters>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
