import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IParamaters } from 'app/shared/model/paramaters.model';
import { ParamatersService } from './paramaters.service';
import { ParamatersDeleteDialogComponent } from './paramaters-delete-dialog.component';

@Component({
  selector: 'jhi-paramaters',
  templateUrl: './paramaters.component.html',
})
export class ParamatersComponent implements OnInit, OnDestroy {
  paramaters?: IParamaters[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected paramatersService: ParamatersService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.paramatersService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IParamaters[]>) => (this.paramaters = res.body || []));
      return;
    }

    this.paramatersService.query().subscribe((res: HttpResponse<IParamaters[]>) => (this.paramaters = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInParamaters();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IParamaters): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInParamaters(): void {
    this.eventSubscriber = this.eventManager.subscribe('paramatersListModification', () => this.loadAll());
  }

  delete(paramaters: IParamaters): void {
    const modalRef = this.modalService.open(ParamatersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.paramaters = paramaters;
  }
}
