import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomSharedModule } from 'app/shared/shared.module';
import { ParamatersComponent } from './paramaters.component';
import { ParamatersDetailComponent } from './paramaters-detail.component';
import { ParamatersUpdateComponent } from './paramaters-update.component';
import { ParamatersDeleteDialogComponent } from './paramaters-delete-dialog.component';
import { paramatersRoute } from './paramaters.route';

@NgModule({
  imports: [EcomSharedModule, RouterModule.forChild(paramatersRoute)],
  declarations: [ParamatersComponent, ParamatersDetailComponent, ParamatersUpdateComponent, ParamatersDeleteDialogComponent],
  entryComponents: [ParamatersDeleteDialogComponent],
})
export class EcomParamatersModule {}
