import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IParamaters } from 'app/shared/model/paramaters.model';
import { ParamatersService } from './paramaters.service';

@Component({
  templateUrl: './paramaters-delete-dialog.component.html',
})
export class ParamatersDeleteDialogComponent {
  paramaters?: IParamaters;

  constructor(
    protected paramatersService: ParamatersService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.paramatersService.delete(id).subscribe(() => {
      this.eventManager.broadcast('paramatersListModification');
      this.activeModal.close();
    });
  }
}
