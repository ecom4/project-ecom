import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IParamaters } from 'app/shared/model/paramaters.model';

@Component({
  selector: 'jhi-paramaters-detail',
  templateUrl: './paramaters-detail.component.html',
})
export class ParamatersDetailComponent implements OnInit {
  paramaters: IParamaters | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paramaters }) => (this.paramaters = paramaters));
  }

  previousState(): void {
    window.history.back();
  }
}
