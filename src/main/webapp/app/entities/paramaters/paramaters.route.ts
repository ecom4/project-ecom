import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IParamaters, Paramaters } from 'app/shared/model/paramaters.model';
import { ParamatersService } from './paramaters.service';
import { ParamatersComponent } from './paramaters.component';
import { ParamatersDetailComponent } from './paramaters-detail.component';
import { ParamatersUpdateComponent } from './paramaters-update.component';

@Injectable({ providedIn: 'root' })
export class ParamatersResolve implements Resolve<IParamaters> {
  constructor(private service: ParamatersService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IParamaters> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((paramaters: HttpResponse<Paramaters>) => {
          if (paramaters.body) {
            return of(paramaters.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Paramaters());
  }
}

export const paramatersRoute: Routes = [
  {
    path: '',
    component: ParamatersComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.paramaters.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ParamatersDetailComponent,
    resolve: {
      paramaters: ParamatersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.paramaters.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ParamatersUpdateComponent,
    resolve: {
      paramaters: ParamatersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.paramaters.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ParamatersUpdateComponent,
    resolve: {
      paramaters: ParamatersResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.paramaters.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
