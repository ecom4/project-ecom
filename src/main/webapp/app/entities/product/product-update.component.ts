import { AppSettings } from './../../settings';
import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductService } from './product.service';
import { ITag } from 'app/shared/model/tag.model';
import { TagService } from 'app/entities/tag/tag.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Cloudinary } from '@cloudinary/angular-5.x';

type SelectableEntity = ITag | ICategory;

@Component({
  selector: 'jhi-product-update',
  templateUrl: './product-update.component.html',
})
export class ProductUpdateComponent implements OnInit {
  isSaving = false;
  tags: ITag[] = [];
  categories: ICategory[] = [];
  imageFile: File | null = null;
  public uploader: FileUploader = new FileUploader({});
  imageUrl = '';

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    stock: [null, [Validators.required, Validators.min(0)]],
    price: [null, [Validators.required, Validators.min(0.0)]],
    discount: [null, [Validators.min(0.0), Validators.max(100.0)]],
    kiloWeight: [null, [Validators.required, Validators.min(0.0)]],
    imageUrl: [],
    description: [],
    tags: [],
    category: [null, Validators.required],
  });

  constructor(
    protected productService: ProductService,
    protected tagService: TagService,
    protected categoryService: CategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private cloudinary: Cloudinary
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product }) => {
      this.updateForm(product);

      this.tagService.query().subscribe((res: HttpResponse<ITag[]>) => (this.tags = res.body || []));

      this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => (this.categories = res.body || []));
    });

    // FROM: cloudinary/cloudinary_angular@github
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/upload`,
      autoUpload: true,
      isHTML5: true,
      removeAfterUpload: true,
      headers: [
        {
          name: 'X-Requested-With',
          value: 'XMLHttpRequest',
        },
      ],
    };

    this.uploader = new FileUploader(uploaderOptions);

    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      // Add Cloudinary unsigned upload preset to the upload form
      form.append('upload_preset', this.cloudinary.config().upload_preset);
      form.append('api_key', this.cloudinary.config().api_key);
      form.append('api_secret', this.cloudinary.config().api_secret);

      // Add file to upload
      form.append('file', fileItem);

      // Use default "withCredentials" value for CORS requests
      fileItem.withCredentials = false;
      return { fileItem, form };
    };

    this.uploader.onCompleteItem = (item: FileItem, response: string): any => {
      const resp = JSON.parse(response);
      if (resp.url) {
        this.imageUrl = resp.url.replace(AppSettings.IMG_ENDPOINT, '');
      }
    };
  }

  get currentImageUrl(): string {
    if (this.imageUrl) {
      return AppSettings.IMG_ENDPOINT + this.imageUrl;
    }

    return '';
  }

  updateForm(product: IProduct): void {
    this.editForm.patchValue({
      id: product.id,
      name: product.name,
      stock: product.stock,
      price: product.price,
      discount: product.discount,
      kiloWeight: product.kiloWeight,
      imageUrl: product.imageUrl,
      description: product.description,
      tags: product.tags,
      category: product.category,
    });

    this.imageUrl = product.imageUrl ? product.imageUrl : '';
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const product = this.createFromForm();
    this.subscribeToSaveResponse(this.productService.update(product));
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      stock: this.editForm.get(['stock'])!.value,
      price: this.editForm.get(['price'])!.value,
      discount: this.editForm.get(['discount'])!.value,
      kiloWeight: this.editForm.get(['kiloWeight'])!.value,
      imageUrl: this.imageUrl,
      description: this.editForm.get(['description'])!.value,
      tags: this.editForm.get(['tags'])!.value,
      category: this.editForm.get(['category'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: ITag[], option: ITag): ITag {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
