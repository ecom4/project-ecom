import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomSharedModule } from 'app/shared/shared.module';
import { PorderComponent } from './porder.component';
import { PorderDetailComponent } from './porder-detail.component';
import { PorderUpdateComponent } from './porder-update.component';
import { PorderDeleteDialogComponent } from './porder-delete-dialog.component';
import { porderRoute } from './porder.route';

@NgModule({
  imports: [EcomSharedModule, RouterModule.forChild(porderRoute)],
  declarations: [PorderComponent, PorderDetailComponent, PorderUpdateComponent, PorderDeleteDialogComponent],
  entryComponents: [PorderDeleteDialogComponent],
})
export class EcomPorderModule {}
