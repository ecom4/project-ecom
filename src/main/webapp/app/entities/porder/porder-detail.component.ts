import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPorder } from 'app/shared/model/porder.model';

@Component({
  selector: 'jhi-porder-detail',
  templateUrl: './porder-detail.component.html',
})
export class PorderDetailComponent implements OnInit {
  porder: IPorder | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ porder }) => (this.porder = porder));
  }

  previousState(): void {
    window.history.back();
  }
}
