import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { IPorder } from 'app/shared/model/porder.model';

type EntityResponseType = HttpResponse<IPorder>;
type EntityArrayResponseType = HttpResponse<IPorder[]>;

@Injectable({ providedIn: 'root' })
export class PorderService {
  public resourceUrl = SERVER_API_URL + 'api/porders';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/porders';

  constructor(protected http: HttpClient) {}

  create(porder: IPorder): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(porder);
    return this.http
      .post<IPorder>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(porder: IPorder): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(porder);
    return this.http
      .put<IPorder>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPorder>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPorder[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPorder[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(porder: IPorder): IPorder {
    const copy: IPorder = Object.assign({}, porder, {
      date: porder.date && porder.date.isValid() ? porder.date.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((porder: IPorder) => {
        porder.date = porder.date ? moment(porder.date) : undefined;
      });
    }
    return res;
  }
}
