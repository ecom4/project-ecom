import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPorder, Porder } from 'app/shared/model/porder.model';
import { PorderService } from './porder.service';
import { PorderComponent } from './porder.component';
import { PorderDetailComponent } from './porder-detail.component';
import { PorderUpdateComponent } from './porder-update.component';

@Injectable({ providedIn: 'root' })
export class PorderResolve implements Resolve<IPorder> {
  constructor(private service: PorderService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPorder> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((porder: HttpResponse<Porder>) => {
          if (porder.body) {
            return of(porder.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Porder());
  }
}

export const porderRoute: Routes = [
  {
    path: '',
    component: PorderComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.porder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PorderDetailComponent,
    resolve: {
      porder: PorderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.porder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PorderUpdateComponent,
    resolve: {
      porder: PorderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.porder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PorderUpdateComponent,
    resolve: {
      porder: PorderResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ecomApp.porder.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
