import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IPorder, Porder } from 'app/shared/model/porder.model';
import { PorderService } from './porder.service';
import { IProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service.ts';

type SelectableEntity = IProductList | IUser;

@Component({
  selector: 'jhi-porder-update',
  templateUrl: './porder-update.component.html',
})
export class PorderUpdateComponent implements OnInit {
  isSaving = false;
  carts: IProductList[] = [];
  extendedusers: IUser[] = [];
  orderStatuses: String[] = ['En cours de traitement', 'Commande prête au retrait', 'Commande expédiée', 'Terminée', 'Annulée'];

  editForm = this.fb.group({
    id: [],
    date: [],
    paymentType: [],
    deliveryType: [],
    status: [],
    totalPrice: [null, [Validators.min(0.0)]],
    deliveryFees: [null, [Validators.min(0.0)]],
    cart: [],
    user: [],
  });

  constructor(
    protected porderService: PorderService,
    protected productListService: ProductListService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ porder }) => {
      if (!porder.id) {
        const today = moment().startOf('day');
        porder.date = today;
      }

      this.updateForm(porder);

      this.productListService
        .query({ filter: 'porder-is-null' })
        .pipe(
          map((res: HttpResponse<IProductList[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IProductList[]) => {
          if (!porder.cart || !porder.cart.id) {
            this.carts = resBody;
          } else {
            this.productListService
              .find(porder.cart.id)
              .pipe(
                map((subRes: HttpResponse<IProductList>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IProductList[]) => (this.carts = concatRes));
          }
        });

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.extendedusers = res.body || []));
    });
  }

  updateForm(porder: IPorder): void {
    this.editForm.patchValue({
      id: porder.id,
      date: porder.date ? porder.date.format(DATE_TIME_FORMAT) : null,
      paymentType: porder.paymentType,
      deliveryType: porder.deliveryType,
      status: porder.status,
      totalPrice: porder.totalPrice,
      deliveryFees: porder.deliveryFees,
      cart: porder.cart,
      user: porder.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const porder = this.createFromForm();
    if (porder.id !== undefined) {
      this.subscribeToSaveResponse(this.porderService.update(porder));
    } else {
      this.subscribeToSaveResponse(this.porderService.create(porder));
    }
  }

  private createFromForm(): IPorder {
    return {
      ...new Porder(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      paymentType: this.editForm.get(['paymentType'])!.value,
      deliveryType: this.editForm.get(['deliveryType'])!.value,
      status: this.editForm.get(['status'])!.value,
      totalPrice: this.editForm.get(['totalPrice'])!.value,
      deliveryFees: this.editForm.get(['deliveryFees'])!.value,
      cart: this.editForm.get(['cart'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPorder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
