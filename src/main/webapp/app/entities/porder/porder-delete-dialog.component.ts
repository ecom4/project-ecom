import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPorder } from 'app/shared/model/porder.model';
import { PorderService } from './porder.service';

@Component({
  templateUrl: './porder-delete-dialog.component.html',
})
export class PorderDeleteDialogComponent {
  porder?: IPorder;

  constructor(protected porderService: PorderService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.porderService.delete(id).subscribe(() => {
      this.eventManager.broadcast('porderListModification');
      this.activeModal.close();
    });
  }
}
