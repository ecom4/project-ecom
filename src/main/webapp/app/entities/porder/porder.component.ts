import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPorder } from 'app/shared/model/porder.model';
import { PorderService } from './porder.service';
import { PorderDeleteDialogComponent } from './porder-delete-dialog.component';

@Component({
  selector: 'jhi-porder',
  templateUrl: './porder.component.html',
})
export class PorderComponent implements OnInit, OnDestroy {
  porders?: IPorder[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected porderService: PorderService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.porderService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IPorder[]>) => (this.porders = res.body || []));
      return;
    }

    this.porderService.query().subscribe((res: HttpResponse<IPorder[]>) => (this.porders = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPorders();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPorder): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPorders(): void {
    this.eventSubscriber = this.eventManager.subscribe('porderListModification', () => this.loadAll());
  }

  delete(porder: IPorder): void {
    const modalRef = this.modalService.open(PorderDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.porder = porder;
  }
}
