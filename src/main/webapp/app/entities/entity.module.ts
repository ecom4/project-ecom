import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.EcomProductModule),
      },
      {
        path: 'tag',
        loadChildren: () => import('./tag/tag.module').then(m => m.EcomTagModule),
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.EcomCategoryModule),
      },
      {
        path: 'product-line',
        loadChildren: () => import('./product-line/product-line.module').then(m => m.EcomProductLineModule),
      },
      {
        path: 'product-list',
        loadChildren: () => import('./product-list/product-list.module').then(m => m.EcomProductListModule),
      },
      {
        path: 'porder',
        loadChildren: () => import('./porder/porder.module').then(m => m.EcomPorderModule),
      },
      {
        path: 'address',
        loadChildren: () => import('./address/address.module').then(m => m.EcomAddressModule),
      },
      {
        path: 'feed-back',
        loadChildren: () => import('./feed-back/feed-back.module').then(m => m.EcomFeedBackModule),
      },
      {
        path: 'review',
        loadChildren: () => import('./review/review.module').then(m => m.EcomReviewModule),
      },
      {
        path: 'paramaters',
        loadChildren: () => import('./paramaters/paramaters.module').then(m => m.EcomParamatersModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EcomEntityModule {}
