import { int } from 'aws-sdk/clients/datapipeline';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IProductLine } from 'app/shared/model/product-line.model';
import { IProductList } from 'app/shared/model/product-list.model';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ProductLineService } from 'app/entities/product-line/product-line.service';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ParamatersService } from 'app/entities/paramaters/paramaters.service';

@Component({
  selector: 'jhi-shopping-cart-summary',
  templateUrl: './shopping-cart-summary.component.html',
  styleUrls: ['./shopping-cart-summary.component.scss'],
})
export class ShoppingCartSummaryComponent implements OnInit {
  productList?: IProductList;
  productLines?: IProductLine[];
  account!: Account;
  isSaving = false;

  cartSubscription?: Subscription;

  totalCost?: number;
  deliveryCost?: number;
  totalCostWithDelivery?: number;

  @Output()
  totalCostLoad: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  deliveryCostLoad: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    protected productListService: ProductListService,
    protected accountService: AccountService,
    protected productLineService: ProductLineService,
    private eventManager: JhiEventManager,
    private paramatersService: ParamatersService
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        this.account = account;
        if (this.account.cartId) {
          this.reloadProductList(this.account.cartId);
          // If cart modification is broadcast - reload the product list
          this.cartSubscription = this.eventManager.subscribe('cartModification', () => this.reloadProductList(this.account.cartId));
        } else {
          this.productList = undefined; // product list should not be loaded if it is not linked to an account
        }
      }
    });
  }

  private reloadProductList(productListId: int): void {
    this.productListService.find(productListId).subscribe((productListResult: HttpResponse<IProductList>) => {
      if (productListResult && productListResult.body) {
        this.productList = productListResult.body;
        this.productLines = this.productList?.productLines;

        if (this.productList?.id !== undefined) {
          this.productListService.getProductListTotalCost(this.productList.id).subscribe(
            value => {
              if (value.body !== undefined) {
                this.totalCost = value.body;
                this.totalCostLoad.emit(this.totalCost);

                // TODO IF LIVRAISON. IF NOT - DO NOT NEED TO ADD DELIVERY COST
                //                 if(true){
                this.paramatersService.find(1).subscribe(dbParameters => {
                  if (dbParameters.body && dbParameters.body.deliveryFees) {
                    this.deliveryCost = dbParameters.body.deliveryFees;
                    this.deliveryCostLoad.emit(this.deliveryCost);

                    if (this.totalCost !== undefined) {
                      this.totalCostWithDelivery = this.totalCost + this.deliveryCost;
                    }
                  }
                });
                // } else{
                //   this.deliveryCost = 0;
                //   this.deliveryCostLoad.emit(this.deliveryCost);
                // }
              }
            }
            // (totalCalculatedCost: HttpResponse<number>) => this.totalCost = totalCalculatedCost.body
          );
        } else {
          this.totalCost = 0;
        }
      }
    });
  }
}
