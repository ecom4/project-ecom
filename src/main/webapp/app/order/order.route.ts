import { OrderHistoryComponent } from './order-history/order-history.component';
import { Route } from '@angular/router';

import { Authority } from 'app/shared/constants/authority.constants';

export const orderRoute: Route = {
  path: 'order-history',
  component: OrderHistoryComponent,
  data: {
    authorities: [Authority.USER, Authority.ADMIN],
    pageTitle: 'porder.history.title',
  },
};
