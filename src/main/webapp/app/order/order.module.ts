import { OrderHistoryComponent } from './order-history/order-history.component';
import { EcomCoreModule } from './../core/core.module';
import { EcomSharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [EcomSharedModule, EcomCoreModule],
  declarations: [OrderHistoryComponent],
  exports: [OrderHistoryComponent],
})
export class EcomOrderModule {}
