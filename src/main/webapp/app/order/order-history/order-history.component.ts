import { PorderService } from './../../entities/porder/porder.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/core/auth/account.service';
import { UserService } from 'app/core/user/user.service';
import { IPorder } from 'app/shared/model/porder.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss'],
})
export class OrderHistoryComponent implements OnInit {
  orders: IPorder[] = [];
  activeOrderId?: number;

  // private account: Account;

  constructor(
    protected accountService: AccountService,
    protected userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private orderService: PorderService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe(params => {
      if ('lastOrder' in params) {
        this.activeOrderId = params['lastOrder'];
      } else {
        this.activeOrderId = undefined;
      }
    });

    this.accountService.identity().subscribe(account => {
      if (account) {
        this.orderService
          .query({
            page: 0,
            size: 0,
            sort: [],
            user: account.id,
          })
          .subscribe((result: HttpResponse<IPorder[]>) => {
            if (result && result.body) {
              this.orders = result.body;
            }
          });
      } else {
        this.router.navigateByUrl('/404');
      }
    });
  }
}
