import { Moment } from 'moment';
import { IProductList } from 'app/shared/model/product-list.model';
import { IUser } from 'app/core/user/user.model';

export interface IPorder {
  id?: number;
  date?: Moment;
  paymentType?: string;
  deliveryType?: string;
  status?: string;
  totalPrice?: number;
  deliveryFees?: number;
  cart?: IProductList;
  user?: IUser;
}

export class Porder implements IPorder {
  constructor(
    public id?: number,
    public date?: Moment,
    public paymentType?: string,
    public deliveryType?: string,
    public status?: string,
    public totalPrice?: number,
    public deliveryFees?: number,
    public cart?: IProductList,
    public user?: IUser
  ) {}
}
