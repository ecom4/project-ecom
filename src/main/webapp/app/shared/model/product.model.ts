import { ITag } from 'app/shared/model/tag.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IProduct {
  id?: number;
  name?: string;
  stock?: number;
  price?: number;
  discount?: number;
  kiloWeight?: number;
  imageUrl?: string;
  description?: string;
  tags?: ITag[];
  category?: ICategory;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public stock?: number,
    public price?: number,
    public discount?: number,
    public kiloWeight?: number,
    public imageUrl?: string,
    public description?: string,
    public tags?: ITag[],
    public category?: ICategory
  ) {}
}
