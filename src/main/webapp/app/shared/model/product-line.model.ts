import { IProduct } from 'app/shared/model/product.model';
import { IProductList } from 'app/shared/model/product-list.model';

export interface IProductLine {
  id?: number;
  quantity?: number;
  unitPrice?: number;
  product?: IProduct;
  productList?: IProductList;
}

export class ProductLine implements IProductLine {
  constructor(
    public id?: number,
    public quantity?: number,
    public unitPrice?: number,
    public product?: IProduct,
    public productList?: IProductList
  ) {}
}
