import { IUser } from 'app/core/user/user.model';

export interface IReview {
  id?: number;
  stars?: number;
  comments?: string;
  user?: IUser;
}

export class Review implements IReview {
  constructor(public id?: number, public stars?: number, public comments?: string, public user?: IUser) {}
}
