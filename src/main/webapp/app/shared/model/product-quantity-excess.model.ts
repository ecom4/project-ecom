import { IProduct } from './product.model';

export interface IProductQuantityExcessModel {
  stockLeft?: number;
  product?: IProduct;
  originalCost?: number;
  revisedCost?: number;
  oldQuantity?: number;
}

export class ProductQuantityExcessModel implements IProductQuantityExcessModel {
  constructor(
    public stockLeft?: number,
    public product?: IProduct,
    public originalCost?: number,
    public revisedCost?: number,
    public oldQuantity?: number
  ) {}
}
