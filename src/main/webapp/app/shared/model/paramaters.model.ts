export interface IParamaters {
  id?: number;
  nameDescription?: string;
  deliveryFees?: number;
}

export class Paramaters implements IParamaters {
  constructor(public id?: number, public nameDescription?: string, public deliveryFees?: number) {}
}
