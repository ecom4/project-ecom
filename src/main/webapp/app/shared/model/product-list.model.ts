import { IProductLine } from 'app/shared/model/product-line.model';
import { IUser } from 'app/core/user/user.model';

export interface IProductList {
  id?: number;
  productLines?: IProductLine[];
  user?: IUser;
}

export class ProductList implements IProductList {
  constructor(public id?: number, public productLines?: IProductLine[], public user?: IUser) {}
}
