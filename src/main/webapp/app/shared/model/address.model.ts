export interface IAddress {
  id?: number;
  line1?: string;
  line2?: string;
  postCode?: string;
  city?: string;
  country?: string;
}

export class Address implements IAddress {
  constructor(
    public id?: number,
    public line1?: string,
    public line2?: string,
    public postCode?: string,
    public city?: string,
    public country?: string
  ) {}
}
