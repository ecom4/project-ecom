import { IProduct } from 'app/shared/model/product.model';

export interface ICategory {
  id?: number;
  name?: string;
  products?: IProduct[];
  childCategories?: ICategory[];
  parentCategory?: ICategory;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public name?: string,
    public products?: IProduct[],
    public childCategories?: ICategory[],
    public parentCategory?: ICategory
  ) {}
}
