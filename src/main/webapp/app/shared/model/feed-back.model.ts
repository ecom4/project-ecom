import { IUser } from 'app/core/user/user.model';

export interface IFeedBack {
  id?: number;
  message?: string;
  read?: boolean;
  user?: IUser;
}

export class FeedBack implements IFeedBack {
  constructor(public id?: number, public message?: string, public read?: boolean, public user?: IUser) {
    this.read = this.read || false;
  }
}
