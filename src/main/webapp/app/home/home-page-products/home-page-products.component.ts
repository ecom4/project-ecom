import { IProduct } from './../../shared/model/product.model';
import { ProductService } from './../../entities/product/product.service';
import { HomePageProductsService } from './home-page-products.service';
import { Component, Input, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'jhi-home-page-products',
  templateUrl: './home-page-products.component.html',
  styleUrls: ['./home-page-products.component.scss'],
})
/* eslint-disable */
export class HomePageProductsComponent implements OnInit {
  @Input() products: IProduct[] = [];
  filtredProducts: IProduct[] = [];

  constructor(private homePageProductsService: HomePageProductsService, private productService: ProductService) {}

  ngOnInit(): void {
    this.homePageProductsService.searchChanged.pipe(debounceTime(500)).subscribe((search: any) => {
      this.searchProducts(search);
    });
    this.searchProducts();
  }

  searchProducts(search?: string): void {
    this.productService.queryLimit().subscribe((res: HttpResponse<IProduct[]>) => {
      this.products = res.body || [];
      console.log('limit products ', this.products);
      this.filtredProducts = search
        ? this.products.filter(p => p.name!.toLowerCase().includes(search.toString().toLowerCase()))
        : this.products;
    });
  }
}
