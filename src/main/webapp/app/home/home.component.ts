import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ProductService } from 'app/entities/product/product.service';
import { IProduct } from 'app/shared/model/product.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  productsWithLowStock: IProduct[] = [];

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    protected productService: ProductService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.productService.findProductsLowStock().subscribe(responseValue => {
      if (responseValue.body !== undefined && responseValue.body !== null) {
        this.productsWithLowStock = responseValue.body;
      }
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  isAdmin(): boolean {
    return this.accountService.isAuthenticated() && this.accountService.hasAnyAuthority('ROLE_ADMIN');
  }
}
