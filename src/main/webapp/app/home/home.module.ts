import { HomePageProductsComponent } from './home-page-products/home-page-products.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EcomSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { EcomProductModule } from 'app/product/product.module';
@NgModule({
  imports: [EcomSharedModule, RouterModule.forChild([HOME_ROUTE]), EcomProductModule],
  declarations: [HomeComponent, HomePageProductsComponent],
})
export class EcomHomeModule {}
