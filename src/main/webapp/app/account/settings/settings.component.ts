import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiLanguageService } from 'ng-jhipster';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { LANGUAGES } from 'app/core/language/language.constants';
import { Address } from 'app/core/user/address.model';

@Component({
  selector: 'jhi-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  account!: Account;
  success = false;
  languages = LANGUAGES;
  settingsForm = this.fb.group({
    firstName: [undefined, [Validators.required, Validators.maxLength(50)]],
    lastName: [undefined, [Validators.required, Validators.maxLength(50)]],
    email: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    phoneNumber: [undefined, [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/(\d{2}){5}/)]],
    address: this.fb.group({
      line1: [undefined, [Validators.required, Validators.maxLength(255)]],
      line2: [undefined],
      postCode: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      city: [undefined, [Validators.required, Validators.maxLength(50)]],
      country: ['France', [Validators.required]],
    }),
  });

  constructor(private accountService: AccountService, private fb: FormBuilder, private languageService: JhiLanguageService) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        if (!account.address) {
          account.address = new Address();
        }
        this.settingsForm.patchValue({
          firstName: account.firstName,
          lastName: account.lastName,
          email: account.email,
          phoneNumber: account.phoneNumber,
          address: account.address,
        });

        this.account = account;
      }
    });
  }

  save(): void {
    this.success = false;

    this.account.firstName = this.settingsForm.get('firstName')!.value;
    this.account.lastName = this.settingsForm.get('lastName')!.value;
    this.account.email = this.settingsForm.get('email')!.value;
    this.account.phoneNumber = this.settingsForm.get('phoneNumber')!.value;
    this.account.address = this.settingsForm.get('address')!.value;
    this.account.langKey = 'fr';

    this.account.login = this.account.email;

    this.accountService.save(this.account).subscribe(() => {
      this.success = true;

      this.accountService.authenticate(this.account);

      if (this.account.langKey !== this.languageService.getCurrentLanguage()) {
        this.languageService.changeLanguage(this.account.langKey);
      }
    });
  }
}
