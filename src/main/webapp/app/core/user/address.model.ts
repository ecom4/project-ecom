export class Address {
  constructor(public line1?: string, public line2?: string, public postCode?: string, public city?: string, public country?: string) {}
}
