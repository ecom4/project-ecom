import { Route } from '@angular/router';

import { CartComponent } from './cart.component';
import { Authority } from 'app/shared/constants/authority.constants';

export const cartRoute: Route = {
  path: 'cart',
  component: CartComponent,
  data: {
    authorities: [Authority.USER, Authority.ADMIN],
    pageTitle: 'cart.title',
  },
};
