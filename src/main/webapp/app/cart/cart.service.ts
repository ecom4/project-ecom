import { map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AccountService } from '../core/auth/account.service';
import { IProductList, ProductList } from '../shared/model/product-list.model';
import { ProductLineService } from '../entities/product-line/product-line.service';
import { ProductListService } from '../entities/product-list/product-list.service';
import { Observable, of, Subject } from 'rxjs';
import { IProduct, Product } from 'app/shared/model/product.model';
import { IProductLine } from 'app/shared/model/product-line.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Account } from 'app/core/user/account.model';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  STORAGE_CART_KEY = 'cart';
  cartLoaded = false;
  cartChanged: Subject<IProductList> = new Subject<IProductList>();

  constructor(
    protected accountService: AccountService,
    protected productListService: ProductListService,
    protected productLineService: ProductLineService,
    protected http: HttpClient
  ) {}

  findProductLineInCart(product: IProduct, cart?: IProductList): Observable<IProductLine | undefined> {
    if (cart) {
      return of(cart.productLines?.find(pl => pl.product?.id === product?.id));
    } else {
      return new Observable<IProduct>(observer => {
        const resultProduct = this.getLineInCart(product);
        observer.next(resultProduct);
      });
    }
  }

  addProductToCart(productToAdd: IProduct, productQuantity: number): Observable<IProductLine> {
    return this.requestCheckCart().pipe(
      switchMap(result => {
        this.saveDbCart(result).subscribe();

        return new Observable<IProductLine>(observer => {
          if (!this.getLineInCart(productToAdd)) {
            const cart = this.getCart();
            const productLine: IProductLine = {
              quantity: productQuantity,
              unitPrice: productToAdd.price,
              product: productToAdd,
            };

            if (this.accountService.isAuthenticated()) {
              productLine.productList = cart;
              this.productLineService.create(productLine).subscribe(
                resProductLine => {
                  const dbProductLine = resProductLine.body;
                  if (dbProductLine) {
                    cart.productLines = cart.productLines?.concat(dbProductLine);
                    this.saveCartLocally(cart);
                    observer.next(dbProductLine);
                  }
                },
                () => {
                  observer.next(undefined);
                }
              );
            } else {
              cart.productLines = cart.productLines?.concat(productLine);
              this.saveCartLocally(cart);
              observer.next(productLine);
            }
          } else {
            observer.next(undefined);
          }
        });
      })
    );
  }

  updateProductInCart(product: IProduct, productQuantity: number): Observable<IProductLine> {
    return new Observable<IProductLine>(observer => {
      const cart = this.getCart();
      const lineIndex = cart.productLines?.findIndex(elem => elem.product?.id === product.id);
      if (cart.productLines && lineIndex !== undefined && lineIndex >= 0) {
        if (productQuantity === 0) {
          const lineId = cart.productLines[lineIndex].id;
          if (this.accountService.isAuthenticated() && lineId) {
            this.productLineService.delete(lineId).subscribe();
          }
          cart.productLines.splice(lineIndex, 1);
        } else {
          cart.productLines[lineIndex].quantity = productQuantity;
          if (this.accountService.isAuthenticated()) {
            this.productLineService.update(cart.productLines[lineIndex]).subscribe();
          }
        }
        this.saveCartLocally(cart);
        observer.next(cart.productLines[lineIndex]);
      }
    });
  }

  loadCartFromDatabase(account: Account, mergeExistingLocalCart = true): void {
    if (account.cartId) {
      this.productListService.find(account.cartId).subscribe(responseCart => {
        let dbCart = responseCart.body;
        if (dbCart) {
          if (mergeExistingLocalCart) {
            dbCart = this.mergeCarts(dbCart, this.getCart());
          }
          this.saveCartLocally(dbCart);
          this.cartChanged.next(dbCart);
        }
      });
    }
  }

  clearLocalCart(keepId = false): IProductList {
    const cart: IProductList = {
      id: keepId ? this.getCart().id : undefined,
      productLines: [],
      user: undefined,
    };
    this.cartChanged.next(cart);
    this.saveCartLocally(cart);
    return cart;
  }

  private mergeCarts(dbCart: IProductList, localCart: IProductList): IProductList {
    if (localCart.productLines) {
      for (let i = 0; i < localCart.productLines.length; i++) {
        const id = localCart.productLines[i].product?.id;
        const resId = dbCart.productLines?.findIndex(line => line.product?.id === id);
        if (resId !== undefined && resId >= 0 && dbCart.productLines) {
          const quantity1 = dbCart.productLines[resId].quantity;
          const quantity2 = localCart.productLines[i].quantity;
          if (quantity1 && quantity2) {
            dbCart.productLines[resId].quantity = Math.max(quantity1, quantity2);
            this.productLineService.update(dbCart.productLines[resId]);
          }
        } else {
          localCart.productLines[i].productList = dbCart;
          this.productLineService.create(localCart.productLines[i]).subscribe(newLine => {
            if (newLine.body) {
              dbCart.productLines = dbCart.productLines?.concat(newLine.body);
              this.saveCartLocally(dbCart);
              this.cartChanged.next(dbCart);
            }
          });
        }
      }
    }
    return dbCart;
  }

  private getLineInCart(product: IProduct): IProductLine | undefined {
    return this.getCart().productLines?.find(line => line.product?.id === product.id);
  }

  private debug(line: string): void {
    localStorage.setItem('debug', `${localStorage.getItem('debug')} | ${line}`);
  }

  private saveCartLocally(cart: IProductList): void {
    localStorage.setItem(this.STORAGE_CART_KEY, JSON.stringify(cart));
  }

  // Saying the user isnt authenticated when clicking on the home page (th
  getCart(): IProductList {
    const cartString = localStorage.getItem(this.STORAGE_CART_KEY);
    let cart: IProductList;
    if (!cartString) {
      cart = this.clearLocalCart();
    } else {
      cart = JSON.parse(cartString);
    }
    if (cart.id && !this.accountService.isAuthenticated()) {
      return this.clearLocalCart();
    } else {
      return cart;
    }
  }

  getItemsInCart(): number {
    if (!this.getCart()) {
      return 0;
    }

    const reducer = (accumulator: any, currentValue: any) => accumulator + currentValue;
    const quantities = this.getCart().productLines?.map(pl => pl.quantity);
    const total = quantities && quantities.length > 0 ? quantities.reduce(reducer) : 0;

    return total ? total : 0;
  }

  public setProductLinePricesWithRemise(productList: IProductList): Observable<HttpResponse<IProductLine>> {
    if (productList.productLines !== undefined) {
      const productLines = productList.productLines; // TODO: Make sure these are up to date - maybe reload from db?
      for (const productLine of productLines) {
        let calculatedUnitPrice = productLine.unitPrice;
        if (productLine.product !== undefined) {
          const productForProductLine: Product = productLine.product;
          if (productForProductLine.price !== undefined) {
            calculatedUnitPrice = productForProductLine.price;
            if (productForProductLine.discount !== undefined) {
              calculatedUnitPrice = Number.parseFloat(
                this.getUnitPriceForProduct(productForProductLine.price, productForProductLine.discount).toFixed(2)
              );
              // calculatedUnitPrice -= productForProductLine.discount;
            }
          }
          productLine.unitPrice = calculatedUnitPrice;
        }
      }
    }
    return this.productListService.update(productList);
    // TODO : DEBUG TO CHECK
    // return productLines;
  }

  getUnitPriceForProduct(price: number, discountPercentage: number): number {
    return price * ((100 - discountPercentage) / 100);
  }

  checkCart(): Observable<any> {
    return this.requestCheckCart().pipe(map(result => this.saveDbCart(result)));
  }

  private requestCheckCart(): Observable<any> {
    if (this.accountService.isAuthenticated()) {
      return this.accountService.identity().pipe(
        switchMap((account: Account | null) => {
          if (account != null && account.cartId == null) {
            const cart = new ProductList();
            cart.user = account;
            return this.productListService
              .create(cart)
              .pipe(map(result => (result && result.body && result.body.id ? result.body : false)));
          } else if (account != null && account.cartId) {
            return this.productListService
              .find(account.cartId)
              .pipe(map(result => (result && result.body && result.body.id ? result.body : false)));
          }
          return of(false);
        })
      );
    }

    return of(false);
  }

  private saveDbCart(c: any): Observable<any> {
    if (c && c.id) {
      const cart = this.mergeCarts(c, this.getCart());
      this.cartChanged.next(cart);
      this.saveCartLocally(cart);
      return this.accountService.updateCartId(c.id);
    }
    return of(false);
  }
}
