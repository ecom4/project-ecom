import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { CartService } from 'app/cart/cart.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-cart-confirm-delete-dialog',
  templateUrl: './cart-confirm-delete-dialog.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartConfirmDeleteDialogComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager,
    protected productListService: ProductListService,
    protected cartService: CartService,
    protected accountService: AccountService
  ) {}

  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  emptyCart(): void {
    if (this.accountService.isAuthenticated()) {
      this.productListService.updateRemoveProductLines(this.cartService.getCart()).subscribe(() => {
        this.cartService.clearLocalCart(true);
        this.eventManager.broadcast('cartModification');
        this.activeModal.close();
      });
    } else {
      this.cartService.clearLocalCart();
      this.eventManager.broadcast('cartModification');
      this.activeModal.close();
    }
    // this.subscribeToSaveResponse(this.productListService.updateRemoveProductLines(cart));
  }
  //
  //
  // private subscribeToSaveResponse(result: Observable<HttpResponse<IProductList>>): void {
  //   result.subscribe(
  //     () => this.onSaveSuccess(),
  //     () => this.onSaveError()
  //   );
  // }
  //
  // private onSaveSuccess(): void {
  //   // this.eventManager.broadcast('cartModification');
  //
  //

  // // Reload product list
  // if (this.productList && this.productList.id) {
  //   this.reloadProductList(this.productList.id);
  // }
  // }

  // private reloadProductList(productListId: int): void {
  //   this.productListService.find(productListId).subscribe((productListResult: HttpResponse<IProductList>) => {
  //     if (productListResult && productListResult.body) {
  //       this.productList = productListResult.body;
  //       this.productLines = this.productList.productLines;
  //     }
  //   });
  // }

  // private onSaveError(): void {}
}
