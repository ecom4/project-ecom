import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UserService } from 'app/core/user/user.service';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { IProductList } from 'app/shared/model/product-list.model';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { Subscription } from 'rxjs';
import { IProductLine } from 'app/shared/model/product-line.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CartConfirmDeleteDialogComponent } from 'app/cart/cart-confirm-delete-dialog.component';
import { JhiEventManager } from 'ng-jhipster';
import { ProductLineService } from 'app/entities/product-line/product-line.service';
import { ProductDetailsService } from 'app/product/product-details/product-details.service';
import { CartService } from 'app/cart/cart.service';
import { Router } from '@angular/router';
import { LoginModalService } from 'app/core/login/login-modal.service';

@Component({
  selector: 'jhi-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
/* eslint-disable */
export class CartComponent implements OnInit, AfterViewInit {
  // productLines?: ProductLine[] = [];
  err: any;
  productList?: IProductList;
  productLines?: IProductLine[];
  account!: Account;
  isSaving = false;
  totalCost?: number;
  cartSubscription?: Subscription;
  cartProductRemovalSubscription?: Subscription;
  idSelected: boolean = false;
  constructor(
    protected productListService: ProductListService,
    protected accountService: AccountService,
    protected userService: UserService,
    private modalService: NgbModal,
    private eventManager: JhiEventManager,
    protected productLineService: ProductLineService,
    private productDetailsService: ProductDetailsService,
    private cartService: CartService,
    private router: Router,
    private loginModalService: LoginModalService
  ) {}

  ngOnInit(): void {
    // console.log('TEST');
    this.reloadProductList();
    // If cart modification is broadcast - reload the product list
    this.cartSubscription = this.eventManager.subscribe('cartModification', () => this.reloadProductList());
    this.cartProductRemovalSubscription = this.eventManager.subscribe('cartProductRemoval', () => this.selectFirst());
    this.selectFirst();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.selectFirst();
    });
  }

  emptyCart(): void {
    const modalRef = this.modalService.open(CartConfirmDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
  }

  selectFirst(): void {
    if (this.productLines?.length !== undefined && this.productLines?.length >= 1) {
      this.updateIdSelected(this.productLines[0].product, this.productLines[0].quantity);
    }
  }

  updateIdSelected(product: any, quantity: any): void {
    this.idSelected = true;
    this.productDetailsService.productChanged.next(product);
    this.productDetailsService.quantityChanged.next(quantity);
  }

  private reloadProductList(): void {
    this.productList = this.cartService.getCart();
    this.productLines = this.productList.productLines;
    this.totalCost = 0;
    for (const line in this.productLines) {
      let computedPrice = this.productLines[line].product.price;
      if (this.productLines[line].product.discount) {
        // computedPrice = computedPrice * ((100 - this.productLines[line].product.discount) / 100);
        computedPrice = this.cartService.getUnitPriceForProduct(computedPrice, this.productLines[line].product.discount).toFixed(2);
      }
      this.totalCost += computedPrice * this.productLines[line].quantity;
    }
  }

  validateCart(productList?: IProductList) {
    if (productList !== undefined) {
      this.router.navigate(['/payment']).then(() => {
        this.cartService.setProductLinePricesWithRemise(productList).subscribe();
      });
    }
  }

  // removeProductLine(product: IProduct): void {
  //   this.cartService.updateProductInCart(product, 0);
  // }
}
