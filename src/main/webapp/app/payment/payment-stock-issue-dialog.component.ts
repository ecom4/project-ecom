import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { ProductQuantityExcessModel } from 'app/shared/model/product-quantity-excess.model';
import { Router } from '@angular/router';
import { IProductList } from 'app/shared/model/product-list.model';
import { CartService } from 'app/cart/cart.service';
import { ProductLineService } from 'app/entities/product-line/product-line.service';
import { ParamatersService } from 'app/entities/paramaters/paramaters.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-payment-stock-issue-dialog',
  templateUrl: './payment-stock-issue-dialog.component.html',
})
export class PaymentStockIssueDialogComponent implements OnInit {
  excessQuantityErrors?: ProductQuantityExcessModel[];
  originalTotal?: number;
  revisedTotal?: number;
  cart?: IProductList;
  deliveryCost?: number;

  constructor(
    private productLineService: ProductLineService,
    private cartService: CartService,
    public activeModal: NgbActiveModal,
    protected productListService: ProductListService,
    private router: Router,
    private paramatersService: ParamatersService,
    private eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmChanges(): void {
    // TODO : EDIT PRODUCT LINES AND RUN SAVE METHOD ON PAYMENT COMPONENT
    this.activeModal.close();
    for (const excessQuantity of this.excessQuantityErrors || []) {
      if (excessQuantity.product) {
        this.cartService.updateProductInCart(excessQuantity.product, excessQuantity.stockLeft || 0).subscribe(() => {
          this.eventManager.broadcast('cartModification');
        });
      }
    }
    this.eventManager.broadcast('porderSubscription');
    this.router.navigate(['/cart']);
  }

  backToCart(): void {
    this.activeModal.close();
    for (const excessQuantity of this.excessQuantityErrors || []) {
      if (excessQuantity.product) {
        this.cartService.updateProductInCart(excessQuantity.product, 0).subscribe(() => this.eventManager.broadcast('cartModification'));
        this.eventManager.broadcast('cartProductRemoval');
      }
    }
    this.router.navigate(['/cart']);
  }
  //
  //
  // private subscribeToSaveResponse(result: Observable<HttpResponse<IProductList>>): void {
  //   result.subscribe(
  //     () => this.onSaveSuccess(),
  //     () => this.onSaveError()
  //   );
  // }
  //
  // private onSaveSuccess(): void {
  //   // this.eventManager.broadcast('cartModification');
  //
  //

  // // Reload product list
  // if (this.productList && this.productList.id) {
  //   this.reloadProductList(this.productList.id);
  // }
  // }

  // private reloadProductList(productListId: int): void {
  //   this.productListService.find(productListId).subscribe((productListResult: HttpResponse<IProductList>) => {
  //     if (productListResult && productListResult.body) {
  //       this.productList = productListResult.body;
  //       this.productLines = this.productList.productLines;
  //     }
  //   });
  // }

  // private onSaveError(): void {}
}
