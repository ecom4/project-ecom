import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { Address } from 'app/core/user/address.model';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiAlertService, JhiLanguageService, JhiEventManager } from 'ng-jhipster';
import { Porder } from 'app/shared/model/porder.model';
import { ProductListService } from 'app/entities/product-list/product-list.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IProductList } from 'app/shared/model/product-list.model';
import { PorderService } from 'app/entities/porder/porder.service';
import { Observable, Subscription } from 'rxjs';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaymentStockIssueDialogComponent } from 'app/payment/payment-stock-issue-dialog.component';
import { CartService } from 'app/cart/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
/* eslint-disable */
export class PaymentComponent implements OnInit {
  shipping: any = {};
  account!: Account;
  success = false;
  totalCost?: number;
  deliveryCost?: number;
  cart?: IProductList;
  newOrder?: Porder;
  porderSubscription?: Subscription;

  settingsForm = this.fb.group({
    firstName: [undefined, [Validators.required, Validators.maxLength(50)]],
    lastName: [undefined, [Validators.required, Validators.maxLength(50)]],
    address: this.fb.group({
      line1: [undefined, [Validators.required, Validators.maxLength(255)]],
      line2: [undefined],
      postCode: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      city: [undefined, [Validators.required, Validators.maxLength(50)]],
      country: ['France', [Validators.required]],
    }),
  });

  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private languageService: JhiLanguageService,
    private productListService: ProductListService,
    private porderService: PorderService,
    private modalService: NgbModal,
    private cartService: CartService,
    private alertService: JhiAlertService,
    private router: Router,
    private eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        if (!account.address) {
          account.address = new Address();
        }
        this.settingsForm.patchValue({
          firstName: account.firstName,
          lastName: account.lastName,
          email: account.email,
          phoneNumber: account.phoneNumber,
          address: account.address,
        });

        this.account = account;
        this.porderSubscription = this.eventManager.subscribe('porderSubscription', () => this.save());
      }
    });
  }

  setTotalCost(event: any) {
    this.totalCost = event;
  }

  setDeliveryCost(event: any) {
    this.deliveryCost = event;
  }

  save(): void {
    this.alertService.clear();

    this.success = false;

    this.account.address = this.settingsForm.get('address')!.value;
    this.account.langKey = 'fr';

    this.account.login = this.account.email;

    this.accountService.save(this.account).subscribe(() => {
      this.success = true;

      this.accountService.authenticate(this.account);

      if (this.account.langKey !== this.languageService.getCurrentLanguage()) {
        this.languageService.changeLanguage(this.account.langKey);
      }

      this.cart = this.cartService.getCart();

      this.newOrder = new Porder();
      this.newOrder.user = this.account;
      this.newOrder.cart = this.cart;
      // newOrder.deliveryFees = 5;
      this.newOrder.deliveryFees = this.deliveryCost;
      this.newOrder.totalPrice = this.totalCost;
      this.newOrder.deliveryType = 'Livraison';
      this.newOrder.status = 'En cours de traitement';
      this.newOrder.paymentType = 'CB'; // TODO : SET IN FORM

      // TODO : MAKE SURE DELIVERY DATE IS SET ON THE BACK END
      this.subscribeToSaveResponse(this.porderService.create(this.newOrder));
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Porder>>): void {
    result.subscribe(
      (porderResponse: HttpResponse<Porder>) => this.onSaveSuccess(porderResponse),
      (err: HttpErrorResponse) => this.onSaveError(err)
    );
  }

  protected onSaveSuccess(porderResponse: HttpResponse<Porder>): void {
    // Reload cart locally
    this.accountService.identity(true).subscribe(account => {
      if (account) {
        this.account = account;
        this.cartService.loadCartFromDatabase(this.account, false);
        let porderIdForRoute;
        if (porderResponse.body != null) {
          porderIdForRoute = porderResponse.body.id;
        }
        this.router.navigate(['/order-history'], { queryParams: { lastOrder: porderIdForRoute } });
      }
    });

    // this.router.navigate(['/order-history?active-order-id=' + porderIdForRoute]);
  }

  protected onSaveError(err: HttpErrorResponse): void {
    let modalRef;
    if (err.error.productQuantityExcesses !== undefined) {
      modalRef = this.modalService.open(PaymentStockIssueDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.excessQuantityErrors = err.error.productQuantityExcesses;
      modalRef.componentInstance.originalTotal = (this.totalCost || 0) + (this.deliveryCost || 0);
      let rTotal = this.totalCost || 0;
      for (var excessError of err.error.productQuantityExcesses) {
        rTotal = rTotal - excessError.originalCost + excessError.revisedCost;
      }

      modalRef.componentInstance.revisedTotal = rTotal + (this.deliveryCost || 0);
      modalRef.componentInstance.deliveryCost = this.deliveryCost;
      modalRef.componentInstance.cart = this.cart;
    }
  }
}
