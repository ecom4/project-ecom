import { Route } from '@angular/router';

import { PaymentComponent } from './payment.component';
import { Authority } from '../shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

export const paymentRoute: Route = {
  path: 'payment',
  component: PaymentComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authority.USER, Authority.ADMIN],
    pageTitle: 'payment.title',
  },
};
