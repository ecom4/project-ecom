import { ICategory } from './../../shared/model/category.model';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'app/entities/category/category.service';

@Component({
  selector: 'jhi-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  rootCategories: ICategory[] = [];

  constructor(private catService: CategoryService) {}

  ngOnInit(): void {
    this.initCategories();
  }

  initCategories(): void {
    this.catService.getRootCategories().subscribe(categories => {
      if (categories && categories.body) {
        this.rootCategories = categories.body;
      }
    });
  }
}
