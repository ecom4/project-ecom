import { CartService } from 'app/cart/cart.service';
import { LoginService } from './../../core/login/login.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { ProductService } from 'app/entities/product/product.service';
import { HomePageProductsService } from 'app/home/home-page-products/home-page-products.service';
import { Account } from 'app/core/user/account.model';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, NavigationStart } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'jhi-navbar-user',
  templateUrl: './navbar-user.component.html',
  styleUrls: ['./navbar-user.component.scss'],
})
/* eslint-disable */
export class NavbarUserComponent implements OnInit {
  faUserCircle = faUserCircle;
  faShoppingCart = faShoppingCart;
  faArrowLeft = faArrowLeft;
  faBars = faBars;
  isNavbarCollapsed?: boolean;
  identity: Account | null = null;
  location = '';

  private destroyed$ = new Subject();
  @Output() clickMenu = new EventEmitter<any>();
  @Output() hideMenu = new EventEmitter<any>();

  constructor(
    private homePageProductsService: HomePageProductsService,
    private accountService: AccountService,
    protected productService: ProductService,
    private loginModalService: LoginModalService,
    private loginService: LoginService,
    private router: Router,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.router.events
      .pipe(
        filter((event: any) => event instanceof NavigationStart),
        takeUntil(this.destroyed$)
      )
      .subscribe((event: NavigationStart) => {
        if (event.url == '/cart') {
          this.location = 'cart';
          this.onHideMenu();
        } else if (event.url == '/payment') {
          this.location = 'payment';
          this.onHideMenu();
        } else {
          this.location = '';
        }
      });
  }

  search(query: string): void {
    this.homePageProductsService.searchChanged.next(query);
  }

  onEnter(query: string) {
    this.router.navigate(['/product-page'], { queryParams: { search: query } });
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  get username(): Observable<String> {
    return this.accountService.identity().pipe(
      map(identity => {
        if (identity) {
          return identity.firstName + ' ' + identity.lastName;
        } else {
          return '';
        }
      })
    );
  }

  get itemsInCart(): number | undefined {
    return this.cartService.getItemsInCart();
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  onClickMenu(): void {
    this.clickMenu.emit();
  }

  onHideMenu(): void {
    this.hideMenu.emit();
  }

  login(): void {
    this.loginModalService.open();
  }
}
