import { ProductDetailsService } from './product-details.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';
import { HttpResponse } from '@angular/common/http';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { AppSettings } from 'app/settings';
import { CartService } from 'app/cart/cart.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
/* eslint-disable */
export class ProductDetailsComponent implements OnInit {
  @Input() isSelected: boolean = false;
  product?: any;
  discountedPrice?: string;
  pricePerKilo = '0';
  quantity?: string;
  imgUrl?: string;
  description?: string;
  presentInCart = false;
  numberOfProducts = 0;
  faCartPlus = faCartPlus;
  currentQuantity?: any;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private eventManager: JhiEventManager,
    private productDetailsService: ProductDetailsService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.productDetailsService.productChanged.subscribe((product: any) => {
      this.getProductData(product);
    });
    this.productDetailsService.quantityChanged.subscribe((quantity: any) => {
      this.numberOfProducts = quantity;
    });

    if (this.isSelected === false) {
      const productId = Number(this.route.snapshot.paramMap.get('id'));
      this.productService.find(productId).subscribe((res: HttpResponse<any>) => this.getProductData(res.body));
    }

    this.numberOfProducts = 0;
  }

  addToCart(): void {
    if (this.product) {
      this.cartService.addProductToCart(this.product, 1).subscribe(() => (this.numberOfProducts = 1));
    }
  }

  updateNumberOfProducts(event?: any): void {
    if (this.product) {
      this.cartService
        .updateProductInCart(this.product, this.numberOfProducts)
        .subscribe(() => this.eventManager.broadcast('cartModification'));
      if (this.numberOfProducts == 0 && this.isSelected) {
        this.eventManager.broadcast('cartProductRemoval');
      }
    }
  }

  removeProductFromCart(): void {
    this.numberOfProducts = 0;
    this.updateNumberOfProducts();
  }

  private getProductData(product: any): void {
    this.product = product;

    if (this.product && this.product.price) {
      let computedPrice = this.product.price;
      if (this.product.discount) {
        // computedPrice = this.product.price * ((100 - this.product.discount) / 100);
        computedPrice = this.cartService.getUnitPriceForProduct(this.product.price, this.product.discount);
      }
      this.discountedPrice = computedPrice.toFixed(2);
      if (this.product.kiloWeight) this.pricePerKilo = (computedPrice / this.product.kiloWeight).toFixed(2);
    }

    if (this.product?.imageUrl) {
      this.imgUrl = AppSettings.IMG_ENDPOINT + this.product?.imageUrl;
    } else {
      this.imgUrl = '/content/images/default.png';
    }

    if (this.product) {
      this.cartService.findProductLineInCart(this.product).subscribe(productLine => {
        if (productLine?.quantity) {
          this.numberOfProducts = productLine?.quantity;
        }
      });
    }
    this.description = this.product.description;
  }
}
