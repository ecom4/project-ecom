import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ProductDetailsService {
  productChanged = new Subject();
  quantityChanged = new Subject();
}
