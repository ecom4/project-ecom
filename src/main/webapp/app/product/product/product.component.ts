import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from 'app/shared/model/product.model';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { CartService } from 'app/cart/cart.service';
import { AppSettings } from 'app/settings';
import { IProductList } from 'app/shared/model/product-list.model';

@Component({
  selector: 'jhi-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  @Input() product?: IProduct;
  discountedPrice?: string;
  basePrice?: string;
  pricePerKilo = '0';
  quantity?: string;
  imgUrl?: string;
  description?: string;
  numberOfProducts = 0;
  faCartPlus = faCartPlus;

  constructor(protected cartService: CartService) {}

  ngOnInit(): void {
    if (this.product && this.product.price) {
      let computedPrice = this.product.price;
      if (this.product.discount) {
        // computedPrice = this.product.price * ((100 - this.product.discount) / 100);
        computedPrice = this.cartService.getUnitPriceForProduct(this.product.price, this.product.discount);
      }
      this.discountedPrice = computedPrice.toFixed(2);
      this.basePrice = this.product.price.toFixed(2);

      if (this.product.kiloWeight) this.pricePerKilo = (computedPrice / this.product.kiloWeight).toFixed(2);

      if (this.product.kiloWeight) {
        if (this.product.kiloWeight >= 1) {
          this.quantity = this.product.kiloWeight + ' kg';
        } else {
          this.quantity = this.product.kiloWeight * 1000 + ' g';
        }
      }
    }

    if (this.product?.imageUrl) {
      this.imgUrl = AppSettings.IMG_ENDPOINT + this.product?.imageUrl;
    } else {
      this.imgUrl = '/content/images/default.png';
    }

    this.description = this.product?.description;

    this.loadNumberOfProductsFromCart(this.cartService.getCart());

    this.cartService.cartChanged.subscribe(cart => {
      this.loadNumberOfProductsFromCart(cart);
    });
  }

  loadNumberOfProductsFromCart(cart: IProductList): void {
    this.numberOfProducts = 0;
    if (this.product && cart) {
      this.cartService.findProductLineInCart(this.product, cart).subscribe(productLine => {
        if (productLine?.quantity) {
          this.numberOfProducts = productLine?.quantity;
        }
      });
    }
  }

  addToCart(): void {
    if (this.product) {
      this.cartService.addProductToCart(this.product, 1).subscribe(productLine => {
        if (productLine) {
          this.numberOfProducts = 1;
        }
      });
    }
  }

  removeProductFromCart(): void {
    this.numberOfProducts = 0;
    this.updateNumberOfProducts();
  }

  updateNumberOfProducts(): void {
    if (this.product) {
      this.cartService.updateProductInCart(this.product, this.numberOfProducts).subscribe();
    }
  }
}
