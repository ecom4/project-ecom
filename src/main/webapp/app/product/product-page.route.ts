import { Routes } from '@angular/router';

import { ProductPageComponent } from './product-page/product-page.component';
import { ProductDetailsComponent } from './product-details/product-details.component';

export const productRoutes: Routes = [
  {
    path: 'product-page',
    component: ProductPageComponent,
    data: {
      pageTitle: 'ecomApp.product.home.title',
    },
  },
  {
    path: 'product-details/:id',
    component: ProductDetailsComponent,
    data: {
      pageTitle: 'ecomApp.product.home.title',
    },
  },
];
