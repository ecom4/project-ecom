import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss'],
})
export class ProductPageComponent implements OnInit {
  response?: any;
  products?: IProduct[];
  page = 1;
  size = 9;
  name?: string;
  pagesNumber = 0;
  productsNumber = 0;
  isSearch = false;
  isCategory = false;
  search = '';
  category = 0;
  sortType = 0;
  sortAtt = 'price';

  constructor(private route: ActivatedRoute, protected productService: ProductService) {}

  ngOnInit(): void {
    this.initializePage();
  }

  initializePage(): void {
    this.page = 1;
    this.sortType = 0;
    this.sortAtt = 'price';
    this.route.queryParams.subscribe(params => {
      if (params['search']) {
        this.isSearch = true;
        this.search = params['search'];
        this.isCategory = false;
        this.category = 0;
      } else if (params['category']) {
        this.isCategory = true;
        this.category = params['category'];
        this.isSearch = false;
        this.search = '';
      } else {
        this.isSearch = false;
        this.isCategory = false;
        this.search = '';
        this.category = 0;
      }

      this.loadData(1);
    });
  }

  loadData(page: number): void {
    this.page = page;
    const params = {
      category: this.category,
      name: this.search,
      page: page - 1,
      size: this.size,
      sortType: this.sortType,
      sortAtt: this.sortAtt,
    };

    this.getProducts(params);
  }

  getProducts(params: any): void {
    this.productService.query(params).subscribe((res: HttpResponse<IProduct[]>) => {
      this.response = res.body;
      this.products = this.response.content || [];
      this.pagesNumber = this.response.totalPages;
      this.productsNumber = this.response.totalElements;
    });
  }

  onChange(): void {
    this.loadData(this.page);
  }
}
