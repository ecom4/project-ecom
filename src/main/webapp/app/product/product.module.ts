import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { EcomSharedModule } from 'app/shared/shared.module';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { NumberPickerModule } from 'ng-number-picker';

@NgModule({
  imports: [EcomSharedModule, NumberPickerModule, BrowserModule, CommonModule, RouterModule],
  declarations: [ProductComponent, ProductListComponent, ProductPageComponent],
  exports: [ProductComponent, ProductListComponent, ProductPageComponent],
})
export class EcomProductModule {}
