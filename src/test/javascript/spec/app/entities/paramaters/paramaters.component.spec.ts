import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EcomTestModule } from '../../../test.module';
import { ParamatersComponent } from 'app/entities/paramaters/paramaters.component';
import { ParamatersService } from 'app/entities/paramaters/paramaters.service';
import { Paramaters } from 'app/shared/model/paramaters.model';

describe('Component Tests', () => {
  describe('Paramaters Management Component', () => {
    let comp: ParamatersComponent;
    let fixture: ComponentFixture<ParamatersComponent>;
    let service: ParamatersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ParamatersComponent],
      })
        .overrideTemplate(ParamatersComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ParamatersComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ParamatersService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Paramaters(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.paramaters && comp.paramaters[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
