import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { ParamatersDetailComponent } from 'app/entities/paramaters/paramaters-detail.component';
import { Paramaters } from 'app/shared/model/paramaters.model';

describe('Component Tests', () => {
  describe('Paramaters Management Detail Component', () => {
    let comp: ParamatersDetailComponent;
    let fixture: ComponentFixture<ParamatersDetailComponent>;
    const route = ({ data: of({ paramaters: new Paramaters(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ParamatersDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ParamatersDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ParamatersDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load paramaters on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.paramaters).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
