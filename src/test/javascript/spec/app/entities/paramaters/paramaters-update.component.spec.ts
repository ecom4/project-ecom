import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { ParamatersUpdateComponent } from 'app/entities/paramaters/paramaters-update.component';
import { ParamatersService } from 'app/entities/paramaters/paramaters.service';
import { Paramaters } from 'app/shared/model/paramaters.model';

describe('Component Tests', () => {
  describe('Paramaters Management Update Component', () => {
    let comp: ParamatersUpdateComponent;
    let fixture: ComponentFixture<ParamatersUpdateComponent>;
    let service: ParamatersService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ParamatersUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ParamatersUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ParamatersUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ParamatersService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Paramaters(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Paramaters();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
