import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { ProductLineUpdateComponent } from 'app/entities/product-line/product-line-update.component';
import { ProductLineService } from 'app/entities/product-line/product-line.service';
import { ProductLine } from 'app/shared/model/product-line.model';

describe('Component Tests', () => {
  describe('ProductLine Management Update Component', () => {
    let comp: ProductLineUpdateComponent;
    let fixture: ComponentFixture<ProductLineUpdateComponent>;
    let service: ProductLineService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ProductLineUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductLineUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductLineUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductLineService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductLine(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductLine();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
