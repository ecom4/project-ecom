import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { ProductLineDetailComponent } from 'app/entities/product-line/product-line-detail.component';
import { ProductLine } from 'app/shared/model/product-line.model';

describe('Component Tests', () => {
  describe('ProductLine Management Detail Component', () => {
    let comp: ProductLineDetailComponent;
    let fixture: ComponentFixture<ProductLineDetailComponent>;
    const route = ({ data: of({ productLine: new ProductLine(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ProductLineDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductLineDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductLineDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productLine on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productLine).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
