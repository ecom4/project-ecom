import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EcomTestModule } from '../../../test.module';
import { ProductLineComponent } from 'app/entities/product-line/product-line.component';
import { ProductLineService } from 'app/entities/product-line/product-line.service';
import { ProductLine } from 'app/shared/model/product-line.model';

describe('Component Tests', () => {
  describe('ProductLine Management Component', () => {
    let comp: ProductLineComponent;
    let fixture: ComponentFixture<ProductLineComponent>;
    let service: ProductLineService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [ProductLineComponent],
      })
        .overrideTemplate(ProductLineComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductLineComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductLineService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProductLine(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.productLines && comp.productLines[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
