import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { PorderUpdateComponent } from 'app/entities/porder/porder-update.component';
import { PorderService } from 'app/entities/porder/porder.service';
import { Porder } from 'app/shared/model/porder.model';

describe('Component Tests', () => {
  describe('Porder Management Update Component', () => {
    let comp: PorderUpdateComponent;
    let fixture: ComponentFixture<PorderUpdateComponent>;
    let service: PorderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [PorderUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PorderUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PorderUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PorderService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Porder(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Porder();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
