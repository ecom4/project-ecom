import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EcomTestModule } from '../../../test.module';
import { PorderDetailComponent } from 'app/entities/porder/porder-detail.component';
import { Porder } from 'app/shared/model/porder.model';

describe('Component Tests', () => {
  describe('Porder Management Detail Component', () => {
    let comp: PorderDetailComponent;
    let fixture: ComponentFixture<PorderDetailComponent>;
    const route = ({ data: of({ porder: new Porder(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [PorderDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PorderDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PorderDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load porder on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.porder).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
