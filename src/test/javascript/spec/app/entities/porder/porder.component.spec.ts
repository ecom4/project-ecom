import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EcomTestModule } from '../../../test.module';
import { PorderComponent } from 'app/entities/porder/porder.component';
import { PorderService } from 'app/entities/porder/porder.service';
import { Porder } from 'app/shared/model/porder.model';

describe('Component Tests', () => {
  describe('Porder Management Component', () => {
    let comp: PorderComponent;
    let fixture: ComponentFixture<PorderComponent>;
    let service: PorderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EcomTestModule],
        declarations: [PorderComponent],
      })
        .overrideTemplate(PorderComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PorderComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PorderService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Porder(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.porders && comp.porders[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
