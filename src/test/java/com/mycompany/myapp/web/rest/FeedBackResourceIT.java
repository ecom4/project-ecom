package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.EcomApp;
import com.mycompany.myapp.domain.FeedBack;
import com.mycompany.myapp.repository.FeedBackRepository;
import com.mycompany.myapp.repository.search.FeedBackSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FeedBackResource} REST controller.
 */
@SpringBootTest(classes = EcomApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class FeedBackResourceIT {

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_READ = false;
    private static final Boolean UPDATED_READ = true;

    @Autowired
    private FeedBackRepository feedBackRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.FeedBackSearchRepositoryMockConfiguration
     */
    @Autowired
    private FeedBackSearchRepository mockFeedBackSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFeedBackMockMvc;

    private FeedBack feedBack;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedBack createEntity(EntityManager em) {
        FeedBack feedBack = new FeedBack()
            .message(DEFAULT_MESSAGE)
            .read(DEFAULT_READ);
        return feedBack;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedBack createUpdatedEntity(EntityManager em) {
        FeedBack feedBack = new FeedBack()
            .message(UPDATED_MESSAGE)
            .read(UPDATED_READ);
        return feedBack;
    }

    @BeforeEach
    public void initTest() {
        feedBack = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeedBack() throws Exception {
        int databaseSizeBeforeCreate = feedBackRepository.findAll().size();
        // Create the FeedBack
        restFeedBackMockMvc.perform(post("/api/feed-backs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feedBack)))
            .andExpect(status().isCreated());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeCreate + 1);
        FeedBack testFeedBack = feedBackList.get(feedBackList.size() - 1);
        assertThat(testFeedBack.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testFeedBack.isRead()).isEqualTo(DEFAULT_READ);

        // Validate the FeedBack in Elasticsearch
        verify(mockFeedBackSearchRepository, times(1)).save(testFeedBack);
    }

    @Test
    @Transactional
    public void createFeedBackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feedBackRepository.findAll().size();

        // Create the FeedBack with an existing ID
        feedBack.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeedBackMockMvc.perform(post("/api/feed-backs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feedBack)))
            .andExpect(status().isBadRequest());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeCreate);

        // Validate the FeedBack in Elasticsearch
        verify(mockFeedBackSearchRepository, times(0)).save(feedBack);
    }


    @Test
    @Transactional
    public void checkMessageIsRequired() throws Exception {
        int databaseSizeBeforeTest = feedBackRepository.findAll().size();
        // set the field null
        feedBack.setMessage(null);

        // Create the FeedBack, which fails.


        restFeedBackMockMvc.perform(post("/api/feed-backs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feedBack)))
            .andExpect(status().isBadRequest());

        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFeedBacks() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get all the feedBackList
        restFeedBackMockMvc.perform(get("/api/feed-backs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedBack.getId().intValue())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].read").value(hasItem(DEFAULT_READ.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        // Get the feedBack
        restFeedBackMockMvc.perform(get("/api/feed-backs/{id}", feedBack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(feedBack.getId().intValue()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.read").value(DEFAULT_READ.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFeedBack() throws Exception {
        // Get the feedBack
        restFeedBackMockMvc.perform(get("/api/feed-backs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        int databaseSizeBeforeUpdate = feedBackRepository.findAll().size();

        // Update the feedBack
        FeedBack updatedFeedBack = feedBackRepository.findById(feedBack.getId()).get();
        // Disconnect from session so that the updates on updatedFeedBack are not directly saved in db
        em.detach(updatedFeedBack);
        updatedFeedBack
            .message(UPDATED_MESSAGE)
            .read(UPDATED_READ);

        restFeedBackMockMvc.perform(put("/api/feed-backs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFeedBack)))
            .andExpect(status().isOk());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeUpdate);
        FeedBack testFeedBack = feedBackList.get(feedBackList.size() - 1);
        assertThat(testFeedBack.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testFeedBack.isRead()).isEqualTo(UPDATED_READ);

        // Validate the FeedBack in Elasticsearch
        verify(mockFeedBackSearchRepository, times(1)).save(testFeedBack);
    }

    @Test
    @Transactional
    public void updateNonExistingFeedBack() throws Exception {
        int databaseSizeBeforeUpdate = feedBackRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeedBackMockMvc.perform(put("/api/feed-backs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(feedBack)))
            .andExpect(status().isBadRequest());

        // Validate the FeedBack in the database
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeedBack in Elasticsearch
        verify(mockFeedBackSearchRepository, times(0)).save(feedBack);
    }

    @Test
    @Transactional
    public void deleteFeedBack() throws Exception {
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);

        int databaseSizeBeforeDelete = feedBackRepository.findAll().size();

        // Delete the feedBack
        restFeedBackMockMvc.perform(delete("/api/feed-backs/{id}", feedBack.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FeedBack> feedBackList = feedBackRepository.findAll();
        assertThat(feedBackList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FeedBack in Elasticsearch
        verify(mockFeedBackSearchRepository, times(1)).deleteById(feedBack.getId());
    }

    @Test
    @Transactional
    public void searchFeedBack() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        feedBackRepository.saveAndFlush(feedBack);
        when(mockFeedBackSearchRepository.search(queryStringQuery("id:" + feedBack.getId())))
            .thenReturn(Collections.singletonList(feedBack));

        // Search the feedBack
        restFeedBackMockMvc.perform(get("/api/_search/feed-backs?query=id:" + feedBack.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedBack.getId().intValue())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].read").value(hasItem(DEFAULT_READ.booleanValue())));
    }
}
