package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.EcomApp;
import com.mycompany.myapp.domain.ProductLine;
import com.mycompany.myapp.domain.Product;
import com.mycompany.myapp.repository.ProductLineRepository;
import com.mycompany.myapp.repository.search.ProductLineSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductLineResource} REST controller.
 */
@SpringBootTest(classes = EcomApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductLineResourceIT {

    public static final Integer DEFAULT_QUANTITY = 0;
    private static final Integer UPDATED_QUANTITY = 1;
    public static final Integer LARGE_QUANTITY = 15;

    private static final Float DEFAULT_UNIT_PRICE = 0.0F;
    private static final Float UPDATED_UNIT_PRICE = 1F;

    @Autowired
    private ProductLineRepository productLineRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ProductLineSearchRepositoryMockConfiguration
     */
    @Autowired
    private ProductLineSearchRepository mockProductLineSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductLineMockMvc;

    private ProductLine productLine;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductLine createEntity(EntityManager em) {
        return createEntity(em, false);
    }

    public static ProductLine createEntity(EntityManager em, boolean largeStock) {
        ProductLine productLine = new ProductLine()
            .quantity(DEFAULT_QUANTITY)
            .unitPrice(DEFAULT_UNIT_PRICE);
        // Add required entity
        Product product;
        if (TestUtil.findAll(em, Product.class).isEmpty()) {
            if(largeStock){
                product = ProductResourceIT.createEntityLargeStock(em);
            } else{
                product = ProductResourceIT.createEntity(em);
            }
            em.persist(product);
            em.flush();
        } else {
            product = TestUtil.findAll(em, Product.class).get(0);
        }
        productLine.setProduct(product);
        return productLine;
    }
    public static ProductLine createEntityLargeQuantity(EntityManager em, boolean largeStock) {
        ProductLine productLine = createEntity(em, largeStock);
        productLine.setQuantity(LARGE_QUANTITY);
        return productLine;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductLine createUpdatedEntity(EntityManager em) {
        ProductLine productLine = new ProductLine()
            .quantity(UPDATED_QUANTITY)
            .unitPrice(UPDATED_UNIT_PRICE);
        // Add required entity
        Product product;
        if (TestUtil.findAll(em, Product.class).isEmpty()) {
            product = ProductResourceIT.createUpdatedEntity(em);
            em.persist(product);
            em.flush();
        } else {
            product = TestUtil.findAll(em, Product.class).get(0);
        }
        productLine.setProduct(product);
        return productLine;
    }

    @BeforeEach
    public void initTest() {
        productLine = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductLine() throws Exception {
        int databaseSizeBeforeCreate = productLineRepository.findAll().size();
        // Create the ProductLine
        restProductLineMockMvc.perform(post("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productLine)))
            .andExpect(status().isCreated());

        // Validate the ProductLine in the database
        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeCreate + 1);
        ProductLine testProductLine = productLineList.get(productLineList.size() - 1);
        assertThat(testProductLine.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testProductLine.getUnitPrice()).isEqualTo(DEFAULT_UNIT_PRICE);

        // Validate the ProductLine in Elasticsearch
        verify(mockProductLineSearchRepository, times(1)).save(testProductLine);
    }

    @Test
    @Transactional
    public void createProductLineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productLineRepository.findAll().size();

        // Create the ProductLine with an existing ID
        productLine.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductLineMockMvc.perform(post("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productLine)))
            .andExpect(status().isBadRequest());

        // Validate the ProductLine in the database
        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeCreate);

        // Validate the ProductLine in Elasticsearch
        verify(mockProductLineSearchRepository, times(0)).save(productLine);
    }


    @Test
    @Transactional
    public void checkQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = productLineRepository.findAll().size();
        // set the field null
        productLine.setQuantity(null);

        // Create the ProductLine, which fails.


        restProductLineMockMvc.perform(post("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productLine)))
            .andExpect(status().isBadRequest());

        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnitPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = productLineRepository.findAll().size();
        // set the field null
        productLine.setUnitPrice(null);

        // Create the ProductLine, which fails.


        restProductLineMockMvc.perform(post("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productLine)))
            .andExpect(status().isBadRequest());

        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductLines() throws Exception {
        // Initialize the database
        productLineRepository.saveAndFlush(productLine);

        // Get all the productLineList
        restProductLineMockMvc.perform(get("/api/product-lines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productLine.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].unitPrice").value(hasItem(DEFAULT_UNIT_PRICE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getProductLine() throws Exception {
        // Initialize the database
        productLineRepository.saveAndFlush(productLine);

        // Get the productLine
        restProductLineMockMvc.perform(get("/api/product-lines/{id}", productLine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productLine.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.unitPrice").value(DEFAULT_UNIT_PRICE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProductLine() throws Exception {
        // Get the productLine
        restProductLineMockMvc.perform(get("/api/product-lines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductLine() throws Exception {
        // Initialize the database
        productLineRepository.saveAndFlush(productLine);

        int databaseSizeBeforeUpdate = productLineRepository.findAll().size();

        // Update the productLine
        ProductLine updatedProductLine = productLineRepository.findById(productLine.getId()).get();
        // Disconnect from session so that the updates on updatedProductLine are not directly saved in db
        em.detach(updatedProductLine);
        updatedProductLine
            .quantity(UPDATED_QUANTITY)
            .unitPrice(UPDATED_UNIT_PRICE);

        restProductLineMockMvc.perform(put("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductLine)))
            .andExpect(status().isOk());

        // Validate the ProductLine in the database
        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeUpdate);
        ProductLine testProductLine = productLineList.get(productLineList.size() - 1);
        assertThat(testProductLine.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testProductLine.getUnitPrice()).isEqualTo(UPDATED_UNIT_PRICE);

        // Validate the ProductLine in Elasticsearch
        verify(mockProductLineSearchRepository, times(1)).save(testProductLine);
    }

    @Test
    @Transactional
    public void updateNonExistingProductLine() throws Exception {
        int databaseSizeBeforeUpdate = productLineRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductLineMockMvc.perform(put("/api/product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productLine)))
            .andExpect(status().isBadRequest());

        // Validate the ProductLine in the database
        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ProductLine in Elasticsearch
        verify(mockProductLineSearchRepository, times(0)).save(productLine);
    }

    @Test
    @Transactional
    public void deleteProductLine() throws Exception {
        // Initialize the database
        productLineRepository.saveAndFlush(productLine);

        int databaseSizeBeforeDelete = productLineRepository.findAll().size();

        // Delete the productLine
        restProductLineMockMvc.perform(delete("/api/product-lines/{id}", productLine.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductLine> productLineList = productLineRepository.findAll();
        assertThat(productLineList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ProductLine in Elasticsearch
        verify(mockProductLineSearchRepository, times(1)).deleteById(productLine.getId());
    }

    @Test
    @Transactional
    public void searchProductLine() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        productLineRepository.saveAndFlush(productLine);
        when(mockProductLineSearchRepository.search(queryStringQuery("id:" + productLine.getId())))
            .thenReturn(Collections.singletonList(productLine));

        // Search the productLine
        restProductLineMockMvc.perform(get("/api/_search/product-lines?query=id:" + productLine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productLine.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].unitPrice").value(hasItem(DEFAULT_UNIT_PRICE.doubleValue())));
    }
}
