package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.EcomApp;
import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.repository.PorderRepository;
import com.mycompany.myapp.repository.ProductListRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.repository.search.PorderSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PorderResource} REST controller.
 */
@SpringBootTest(classes = EcomApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class PorderResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PAYMENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PAYMENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Float DEFAULT_TOTAL_PRICE = 0.0F;
    private static final Float UPDATED_TOTAL_PRICE = 1F;

    private static final Float DEFAULT_DELIVERY_FEES = 0.0F;
    private static final Float UPDATED_DELIVERY_FEES = 1F;

    @Autowired
    private PorderRepository porderRepository;

    @Autowired
    private ProductListRepository productListRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.PorderSearchRepositoryMockConfiguration
     */
    @Autowired
    private PorderSearchRepository mockPorderSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPorderMockMvc;

    private Porder porder;
    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Porder createEntity(EntityManager em) {
        Porder porder = new Porder()
            .date(DEFAULT_DATE)
            .paymentType(DEFAULT_PAYMENT_TYPE)
            .deliveryType(DEFAULT_DELIVERY_TYPE)
            .status(DEFAULT_STATUS)
            .totalPrice(DEFAULT_TOTAL_PRICE)
            .deliveryFees(DEFAULT_DELIVERY_FEES);
        return porder;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Porder createUpdatedEntity(EntityManager em) {
        Porder porder = new Porder()
            .date(UPDATED_DATE)
            .paymentType(UPDATED_PAYMENT_TYPE)
            .deliveryType(UPDATED_DELIVERY_TYPE)
            .status(UPDATED_STATUS)
            .totalPrice(UPDATED_TOTAL_PRICE)
            .deliveryFees(UPDATED_DELIVERY_FEES);
        return porder;
    }

    public static Porder createEntityWithProductList(EntityManager em, boolean withProductLines, boolean includeProductWithLargeStock, boolean includeProductLineWithLargeQuantity) {

        /*
         * Create product list
         */
        ProductList productList;
        if(withProductLines){
            productList = ProductListResourceIT.createEntityWithProductLines(em, includeProductWithLargeStock, includeProductLineWithLargeQuantity);
        } else{
            productList = ProductListResourceIT.createEntity(em);
        }
        em.persist(productList);

        em.flush();

        /*
         * Create user
         */
        User userForOrder;
        if (TestUtil.findAll(em, User.class).isEmpty()) {
            userForOrder = UserResourceIT.createEntity(em);
        } else {
            userForOrder = TestUtil.findAll(em, User.class).get(0);
        }

        /*
         * Associate product list to user (cart)
         */
        userForOrder.setCart(productList);
        em.persist(userForOrder);
        em.flush();

        /*
         * Associate product list to user (cart)
         */
        Porder orderWithProductList = createEntity(em);
        orderWithProductList.setCart(productList);
        orderWithProductList.setUser(userForOrder);


        return orderWithProductList;
    }


        @BeforeEach
    public void initTest() {
        porder = createEntity(em);
    }

//    @Test
//    @Transactional
//    public void createPorder() throws Exception {
//        int databaseSizeBeforeCreate = porderRepository.findAll().size();
//        // Create the Porder
//        restPorderMockMvc.perform(post("/api/porders")
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(porder)))
//            .andExpect(status().isCreated());
//
//        // Validate the Porder in the database
//        List<Porder> porderList = porderRepository.findAll();
//        assertThat(porderList).hasSize(databaseSizeBeforeCreate + 1);
//        Porder testPorder = porderList.get(porderList.size() - 1);
//        assertThat(testPorder.getDate()).isEqualTo(DEFAULT_DATE);
//        assertThat(testPorder.getPaymentType()).isEqualTo(DEFAULT_PAYMENT_TYPE);
//        assertThat(testPorder.getDeliveryType()).isEqualTo(DEFAULT_DELIVERY_TYPE);
//        assertThat(testPorder.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);
//        assertThat(testPorder.getDeliveryFees()).isEqualTo(DEFAULT_DELIVERY_FEES);
//
//        // Validate the Porder in Elasticsearch
//        verify(mockPorderSearchRepository, times(1)).save(testPorder);
//    }

    @Test
    @Transactional
    public void createPorderNoProducts() throws Exception {


        this.porder = createEntityWithProductList(em, false, true, false);

        int databaseSizeBeforeCreate = porderRepository.findAll().size();
        int productListSizeBeforeCreate = productListRepository.findAll().size();


//        int productListSizeForUserBeforeCreation = porder.getUser()

        /*
         * Cannot create porder with no products - an order should always contain products in its associated product list
         */
        restPorderMockMvc.perform(post("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(porder)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.errorKey").value("ecomApp.porder.error.noProducts"));

        /*
         * Verify porder size hasnt changed due to the error
         */
        List<Porder> porderList = porderRepository.findAll();
        List<ProductList> productListList = productListRepository.findAll();

        assertThat(porderList).hasSize(databaseSizeBeforeCreate);
        assertThat(productListList).hasSize(productListSizeBeforeCreate);

    }

    @Test
    @Transactional
    public void createPorderWithProducts() throws Exception {


        /*
         * Change porder to one with products
         */
        this.porder = createEntityWithProductList(em, true, true, false);
        this.porder.setDate(null); // set to the null so we can test that it is set during the API post execution

        int databaseSizeBeforeCreate = porderRepository.findAll().size();
        int productListSizeBeforeCreate = productListRepository.findAll().size();



        /*
         * Save expected stock values after update (stock - product line quantity) this will be used in the assertions
         */
        HashMap<Long, Integer> expectedStockAfterUpdate = new HashMap<>();
        for(ProductLine orderProductLine : porder.getCart().getProductLines()){
            expectedStockAfterUpdate.put(orderProductLine.getId(), (orderProductLine.getProduct().getStock() - orderProductLine.getQuantity()));
        }


        assertThat(porder.getId() == null); // Porder not yet persisted

        // Create the Porder
        restPorderMockMvc.perform(post("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(porder)))
            .andExpect(status().isCreated());
        assertThat(porder.getId() != null); // Porder persisted

        // Validate the Porder and product lists in the database
        List<Porder> porderList = porderRepository.findAll();
        List<ProductList> productListList = productListRepository.findAll();


        // Porder should be created
        assertThat(porderList).hasSize(databaseSizeBeforeCreate + 1);
        // New product list should be created
        assertThat(productListList).hasSize(productListSizeBeforeCreate + 1);


        Porder testPorder = porderList.get(porderList.size() - 1);
        assertThat(testPorder.getPaymentType()).isEqualTo(DEFAULT_PAYMENT_TYPE);
        assertThat(testPorder.getDeliveryType()).isEqualTo(DEFAULT_DELIVERY_TYPE);
        assertThat(testPorder.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);
        assertThat(testPorder.getDeliveryFees()).isEqualTo(DEFAULT_DELIVERY_FEES);
        assertThat(testPorder.getCart()).isNotNull();
        assertThat(testPorder.getDate()).isNotNull(); // Set on prepersist


        User testUser = userRepository.findById(testPorder.getUser().getId()).orElse(null);

        // Order cart (product list) should be the product list with product lines. User cart should be the new empty product list
        assertThat(testPorder.getCart().getId()).isNotEqualTo(testUser.getCart().getId());
        assertThat(testPorder.getCart()).isNotNull();
        assertThat(testPorder.getCart().getProductLines()).isNotEmpty();
        assertThat(testUser.getCart()).isNotNull();
        assertThat(testUser.getCart().getProductLines()).isEmpty();


        // Check stock has gone down for product (equal to the stock - product line quantity for each product line
        assertThat(porder.getCart().getProductLines().size()).isEqualTo(expectedStockAfterUpdate.size()); // Assert that the product lines have not changed
        for(ProductLine productLine : porder.getCart().getProductLines()){
            assertThat(expectedStockAfterUpdate).containsKey(productLine.getId()); // Assert that the product lines have not changed
            assertThat(productLine.getProduct().getStock()).isEqualTo(expectedStockAfterUpdate.get(productLine.getId())); // Stock should have gone down by the quantity in each product line
        }

        // Validate the Porder in Elasticsearch
        verify(mockPorderSearchRepository, times(1)).save(testPorder);
    }


    /*
     * Order with a quantity of a product that is too large (more than what is in stock). This should return an error 400
     * as the order cannot be processed if this is the case.
     */
    @Test
    @Transactional
    public void createPorderWithProductsExcess() throws Exception {




        /*
         * Change porder to one with products
         */
        this.porder = createEntityWithProductList(em, true, true, true);


        int databaseSizeBeforeCreate = porderRepository.findAll().size();
        int productListSizeBeforeCreate = productListRepository.findAll().size();

        // Create the Porder
        restPorderMockMvc.perform(post("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(porder)))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.productQuantityExcesses").isNotEmpty())
            .andExpect(jsonPath("$.productQuantityExcesses.[*].product").isNotEmpty()) // A product object should be returned - this is the product with the excess quantity
            .andExpect(jsonPath("$.productQuantityExcesses.[*].stockLeft").value(hasItem(ProductResourceIT.LARGE_STOCK))) // Excess quantity should be quantity - the amount of stock
            .andExpect(jsonPath("$.title").value(equalTo("Not enough stock")));

        List<Porder> porderList = porderRepository.findAll();

        List<ProductList> productListList = productListRepository.findAll();
        assertThat(porderList).hasSize(databaseSizeBeforeCreate); // Verify porder size hasnt changed due to the error
        assertThat(productListList).hasSize(productListSizeBeforeCreate);

    }

    // TODO : TEST MAKE SURE PRODCUT STOCK CANNOT BE UPDATED UNTIL TRANSACTION FINISHED

    @Test
    @Transactional
    public void createPorderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = porderRepository.findAll().size();

        // Create the Porder with an existing ID
        porder.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPorderMockMvc.perform(post("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(porder)))
            .andExpect(status().isBadRequest());

        // Validate the Porder in the database
        List<Porder> porderList = porderRepository.findAll();
        assertThat(porderList).hasSize(databaseSizeBeforeCreate);

        // Validate the Porder in Elasticsearch
        verify(mockPorderSearchRepository, times(0)).save(porder);
    }


    @Test
    @Transactional
    public void getAllPorders() throws Exception {
        // Initialize the database
        porderRepository.saveAndFlush(porder);

        // Get all the porderList
        restPorderMockMvc.perform(get("/api/porders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(porder.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").isNotEmpty())
            .andExpect(jsonPath("$.[*].paymentType").value(hasItem(DEFAULT_PAYMENT_TYPE)))
            .andExpect(jsonPath("$.[*].deliveryType").value(hasItem(DEFAULT_DELIVERY_TYPE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryFees").value(hasItem(DEFAULT_DELIVERY_FEES.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getPorder() throws Exception {
        // Initialize the database
        porderRepository.saveAndFlush(porder);

        // Get the porder
        restPorderMockMvc.perform(get("/api/porders/{id}", porder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(porder.getId().intValue()))
            .andExpect(jsonPath("$.date").isNotEmpty())
            .andExpect(jsonPath("$.paymentType").value(DEFAULT_PAYMENT_TYPE))
            .andExpect(jsonPath("$.deliveryType").value(DEFAULT_DELIVERY_TYPE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.totalPrice").value(DEFAULT_TOTAL_PRICE.doubleValue()))
            .andExpect(jsonPath("$.deliveryFees").value(DEFAULT_DELIVERY_FEES.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPorder() throws Exception {
        // Get the porder
        restPorderMockMvc.perform(get("/api/porders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePorder() throws Exception {
        // Initialize the database
        porderRepository.saveAndFlush(porder);

        int databaseSizeBeforeUpdate = porderRepository.findAll().size();

        // Update the porder
        Porder updatedPorder = porderRepository.findById(porder.getId()).get();
        // Disconnect from session so that the updates on updatedPorder are not directly saved in db
        em.detach(updatedPorder);
        updatedPorder
            .date(UPDATED_DATE)
            .paymentType(UPDATED_PAYMENT_TYPE)
            .deliveryType(UPDATED_DELIVERY_TYPE)
            .status(UPDATED_STATUS)
            .totalPrice(UPDATED_TOTAL_PRICE)
            .deliveryFees(UPDATED_DELIVERY_FEES);

        restPorderMockMvc.perform(put("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPorder)))
            .andExpect(status().isOk());

        // Validate the Porder in the database
        List<Porder> porderList = porderRepository.findAll();
        assertThat(porderList).hasSize(databaseSizeBeforeUpdate);
        Porder testPorder = porderList.get(porderList.size() - 1);
        assertThat(testPorder.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testPorder.getPaymentType()).isEqualTo(UPDATED_PAYMENT_TYPE);
        assertThat(testPorder.getDeliveryType()).isEqualTo(UPDATED_DELIVERY_TYPE);
        assertThat(testPorder.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPorder.getTotalPrice()).isEqualTo(UPDATED_TOTAL_PRICE);
        assertThat(testPorder.getDeliveryFees()).isEqualTo(UPDATED_DELIVERY_FEES);

        // Validate the Porder in Elasticsearch
        verify(mockPorderSearchRepository, times(1)).save(testPorder);
    }

    @Test
    @Transactional
    public void updateNonExistingPorder() throws Exception {
        int databaseSizeBeforeUpdate = porderRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPorderMockMvc.perform(put("/api/porders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(porder)))
            .andExpect(status().isBadRequest());

        // Validate the Porder in the database
        List<Porder> porderList = porderRepository.findAll();
        assertThat(porderList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Porder in Elasticsearch
        verify(mockPorderSearchRepository, times(0)).save(porder);
    }

    @Test
    @Transactional
    public void deletePorder() throws Exception {
        // Initialize the database
        porderRepository.saveAndFlush(porder);

        int databaseSizeBeforeDelete = porderRepository.findAll().size();

        // Delete the porder
        restPorderMockMvc.perform(delete("/api/porders/{id}", porder.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Porder> porderList = porderRepository.findAll();
        assertThat(porderList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Porder in Elasticsearch
        verify(mockPorderSearchRepository, times(1)).deleteById(porder.getId());
    }

    @Test
    @Transactional
    public void searchPorder() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        porderRepository.saveAndFlush(porder);
        when(mockPorderSearchRepository.search(queryStringQuery("id:" + porder.getId())))
            .thenReturn(Collections.singletonList(porder));

        // Search the porder
        restPorderMockMvc.perform(get("/api/_search/porders?query=id:" + porder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(porder.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").isNotEmpty())
            .andExpect(jsonPath("$.[*].paymentType").value(hasItem(DEFAULT_PAYMENT_TYPE)))
            .andExpect(jsonPath("$.[*].deliveryType").value(hasItem(DEFAULT_DELIVERY_TYPE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryFees").value(hasItem(DEFAULT_DELIVERY_FEES.doubleValue())));
    }
}
