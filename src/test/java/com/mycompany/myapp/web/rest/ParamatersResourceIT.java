package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.EcomApp;
import com.mycompany.myapp.domain.Paramaters;
import com.mycompany.myapp.repository.ParamatersRepository;
import com.mycompany.myapp.repository.search.ParamatersSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ParamatersResource} REST controller.
 */
@SpringBootTest(classes = EcomApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ParamatersResourceIT {

    private static final String DEFAULT_NAME_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_NAME_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_DELIVERY_FEES = 1D;
    private static final Double UPDATED_DELIVERY_FEES = 2D;

    @Autowired
    private ParamatersRepository paramatersRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.ParamatersSearchRepositoryMockConfiguration
     */
    @Autowired
    private ParamatersSearchRepository mockParamatersSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restParamatersMockMvc;

    private Paramaters paramaters;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paramaters createEntity(EntityManager em) {
        Paramaters paramaters = new Paramaters()
            .nameDescription(DEFAULT_NAME_DESCRIPTION)
            .deliveryFees(DEFAULT_DELIVERY_FEES);
        return paramaters;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paramaters createUpdatedEntity(EntityManager em) {
        Paramaters paramaters = new Paramaters()
            .nameDescription(UPDATED_NAME_DESCRIPTION)
            .deliveryFees(UPDATED_DELIVERY_FEES);
        return paramaters;
    }

    @BeforeEach
    public void initTest() {
        paramaters = createEntity(em);
    }

    @Test
    @Transactional
    public void createParamaters() throws Exception {
        int databaseSizeBeforeCreate = paramatersRepository.findAll().size();
        // Create the Paramaters
        restParamatersMockMvc.perform(post("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paramaters)))
            .andExpect(status().isCreated());

        // Validate the Paramaters in the database
        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeCreate + 1);
        Paramaters testParamaters = paramatersList.get(paramatersList.size() - 1);
        assertThat(testParamaters.getNameDescription()).isEqualTo(DEFAULT_NAME_DESCRIPTION);
        assertThat(testParamaters.getDeliveryFees()).isEqualTo(DEFAULT_DELIVERY_FEES);

        // Validate the Paramaters in Elasticsearch
        verify(mockParamatersSearchRepository, times(1)).save(testParamaters);
    }

    @Test
    @Transactional
    public void createParamatersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paramatersRepository.findAll().size();

        // Create the Paramaters with an existing ID
        paramaters.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParamatersMockMvc.perform(post("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paramaters)))
            .andExpect(status().isBadRequest());

        // Validate the Paramaters in the database
        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeCreate);

        // Validate the Paramaters in Elasticsearch
        verify(mockParamatersSearchRepository, times(0)).save(paramaters);
    }


    @Test
    @Transactional
    public void checkNameDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = paramatersRepository.findAll().size();
        // set the field null
        paramaters.setNameDescription(null);

        // Create the Paramaters, which fails.


        restParamatersMockMvc.perform(post("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paramaters)))
            .andExpect(status().isBadRequest());

        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDeliveryFeesIsRequired() throws Exception {
        int databaseSizeBeforeTest = paramatersRepository.findAll().size();
        // set the field null
        paramaters.setDeliveryFees(null);

        // Create the Paramaters, which fails.


        restParamatersMockMvc.perform(post("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paramaters)))
            .andExpect(status().isBadRequest());

        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParamaters() throws Exception {
        // Initialize the database
        paramatersRepository.saveAndFlush(paramaters);

        // Get all the paramatersList
        restParamatersMockMvc.perform(get("/api/paramaters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paramaters.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameDescription").value(hasItem(DEFAULT_NAME_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].deliveryFees").value(hasItem(DEFAULT_DELIVERY_FEES.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getParamaters() throws Exception {
        // Initialize the database
        paramatersRepository.saveAndFlush(paramaters);

        // Get the paramaters
        restParamatersMockMvc.perform(get("/api/paramaters/{id}", paramaters.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(paramaters.getId().intValue()))
            .andExpect(jsonPath("$.nameDescription").value(DEFAULT_NAME_DESCRIPTION))
            .andExpect(jsonPath("$.deliveryFees").value(DEFAULT_DELIVERY_FEES.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingParamaters() throws Exception {
        // Get the paramaters
        restParamatersMockMvc.perform(get("/api/paramaters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParamaters() throws Exception {
        // Initialize the database
        paramatersRepository.saveAndFlush(paramaters);

        int databaseSizeBeforeUpdate = paramatersRepository.findAll().size();

        // Update the paramaters
        Paramaters updatedParamaters = paramatersRepository.findById(paramaters.getId()).get();
        // Disconnect from session so that the updates on updatedParamaters are not directly saved in db
        em.detach(updatedParamaters);
        updatedParamaters
            .nameDescription(UPDATED_NAME_DESCRIPTION)
            .deliveryFees(UPDATED_DELIVERY_FEES);

        restParamatersMockMvc.perform(put("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedParamaters)))
            .andExpect(status().isOk());

        // Validate the Paramaters in the database
        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeUpdate);
        Paramaters testParamaters = paramatersList.get(paramatersList.size() - 1);
        assertThat(testParamaters.getNameDescription()).isEqualTo(UPDATED_NAME_DESCRIPTION);
        assertThat(testParamaters.getDeliveryFees()).isEqualTo(UPDATED_DELIVERY_FEES);

        // Validate the Paramaters in Elasticsearch
        verify(mockParamatersSearchRepository, times(1)).save(testParamaters);
    }

    @Test
    @Transactional
    public void updateNonExistingParamaters() throws Exception {
        int databaseSizeBeforeUpdate = paramatersRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParamatersMockMvc.perform(put("/api/paramaters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(paramaters)))
            .andExpect(status().isBadRequest());

        // Validate the Paramaters in the database
        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Paramaters in Elasticsearch
        verify(mockParamatersSearchRepository, times(0)).save(paramaters);
    }

    @Test
    @Transactional
    public void deleteParamaters() throws Exception {
        // Initialize the database
        paramatersRepository.saveAndFlush(paramaters);

        int databaseSizeBeforeDelete = paramatersRepository.findAll().size();

        // Delete the paramaters
        restParamatersMockMvc.perform(delete("/api/paramaters/{id}", paramaters.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Paramaters> paramatersList = paramatersRepository.findAll();
        assertThat(paramatersList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Paramaters in Elasticsearch
        verify(mockParamatersSearchRepository, times(1)).deleteById(paramaters.getId());
    }

    @Test
    @Transactional
    public void searchParamaters() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        paramatersRepository.saveAndFlush(paramaters);
        when(mockParamatersSearchRepository.search(queryStringQuery("id:" + paramaters.getId())))
            .thenReturn(Collections.singletonList(paramaters));

        // Search the paramaters
        restParamatersMockMvc.perform(get("/api/_search/paramaters?query=id:" + paramaters.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paramaters.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameDescription").value(hasItem(DEFAULT_NAME_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].deliveryFees").value(hasItem(DEFAULT_DELIVERY_FEES.doubleValue())));
    }
}
