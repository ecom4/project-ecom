package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class ParamatersTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Paramaters.class);
        Paramaters paramaters1 = new Paramaters();
        paramaters1.setId(1L);
        Paramaters paramaters2 = new Paramaters();
        paramaters2.setId(paramaters1.getId());
        assertThat(paramaters1).isEqualTo(paramaters2);
        paramaters2.setId(2L);
        assertThat(paramaters1).isNotEqualTo(paramaters2);
        paramaters1.setId(null);
        assertThat(paramaters1).isNotEqualTo(paramaters2);
    }
}
