package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class PorderTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Porder.class);
        Porder porder1 = new Porder();
        porder1.setId(1L);
        Porder porder2 = new Porder();
        porder2.setId(porder1.getId());
        assertThat(porder1).isEqualTo(porder2);
        porder2.setId(2L);
        assertThat(porder1).isNotEqualTo(porder2);
        porder1.setId(null);
        assertThat(porder1).isNotEqualTo(porder2);
    }
}
